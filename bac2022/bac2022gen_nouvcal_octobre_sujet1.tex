% !TeX TXS-program:compile = txs:///pdflatex

\documentclass[french,a4paper,11pt]{article}
\usepackage[margin=2cm,includeheadfoot]{geometry}
\usepackage{ProfLycee}
\RequirePackage[upright]{fourier}
\usepackage{amsmath,amssymb,amstext}
\usepackage[scaled=0.875]{helvet}
\renewcommand\ttdefault{lmtt}
\usepackage{decimalcomma}
\usepackage{esvect}
\usepackage{tabularx}
\usepackage{wrapstuff}
\usepackage{logoetalab}

\newcommand{\session}{2022}
\newcommand{\annee}{2022}
\newcommand{\serie}{Gé.}
\newcommand{\lieu}{Nouv.-Calédonie}
\newcommand{\jour}{26}
\newcommand{\mois}{Octobre}
\newcommand{\numsujet}{1}
\newcommand{\nomfichier}{[BAC \serie{} \annee{}] \lieu{} (\mois{} \annee)}

\title{\nomfichier}
\usepackage{hyperref}
\usepackage{lastpage}
\usepackage{enumitem}
\usepackage{ibrackets}
\usepackage{fancyhdr}
\usepackage{babel}

\hypersetup{pdfauthor={Pierquet},pdftitle={\nomfichier},allbordercolors=white,pdfborder=0 0 0,pdfstartview=FitH}
\lhead{\scriptsize\sffamily BAC \serie{} \session}
\chead{\scriptsize\sffamily \lieu{} - Sujet \numsujet}
\rhead{\scriptsize\sffamily \jour{} \mois{} \annee{}}
\lfoot{\scriptsize\sffamily \affloetalab[Legende,TexteLegende={Licence Etalab 2.0}]}
\cfoot{\scriptsize\sffamily 22-MATJ1NC1}
\rfoot{\scriptsize\sffamily - \thepage{}/{}\pageref{LastPage} -}
\setlength{\parindent}{0pt}
%divers
\DeclareMathSymbol{;}\mathbin{operators}{'73}
\newcommand\Rij{\left(O;\vect{\imath},\,\vect{\jmath}\right)}
\newcommand\Rijk{\left(O;\vect{\imath},\,\vect{\jmath},\,\vect{k}\right)}
\newcommand\intervOO[2]{\left]#1;#2\right[}
\newcommand\intervFF[2]{\left[#1;#2\right]}
\newcommand\intervOF[2]{\left]#1;#2\right]}
\newcommand\intervFO[2]{\left[#1;#2\right[}
\newcommand\R{\mathbb{R}}
\newcommand\e{\text{e}}
\newcommand\pta[1]{(#1)}
\newcommand\suiten[1][]{\left(#1_n\right)}
\newcommand\vect[1]{\vv{#1}}
\usepackage{forest}
\forestset{
	fleche/.style = {edge={thick}},
	aproba/.style = {edge label = {node[above=6pt,pos=.5,fill=white] {$#1$}}},%poids au-dessus
	bproba/.style = {edge label = {node[below=6pt,pos=.5,fill=white] {$#1$}}},%poids en-dessous
}

\begin{document}

\pagestyle{fancy}

{\hfill\Huge \faGraduationCap\hfill~}

\part*{\lieu{}, Bac \serie{}, \jour{} \mois{} \annee, sujet n°\numsujet}

\vspace{0.25cm}

\hrulefill~\textsf{Le candidat traite trois des \textbf{4 exercices} proposés.}~\hrulefill

\vspace{0.25cm}

\section*{Exercice 1 [Thèmes : fonctions, suites]\dotfill(7 points)} %exo1

\medskip

On considère la fonction $f$ définie sur l'intervalle $\intervOO{0}{+\infty}$ par \[f(x) = x^2 - 6x + 4\ln (x).\]
On admet que la fonction $f$ est deux fois dérivable sur l'intervalle $\intervOO{0}{+\infty}$.

On note $f$' sa dérivée et $f''$ sa dérivée seconde.

On note $\mathcal{C}_f$ la courbe représentative de la fonction $f$ dans un repère orthogonal.

\begin{enumerate}
	\item 
	\begin{enumerate}
		\item Déterminer $\displaystyle\lim_{x \to 0} f(x)$. 
		
		Interpréter graphiquement ce résultat.
		\item Déterminer $\displaystyle\lim_{x \to + \infty} f(x)$.
	\end{enumerate}
	\item
	\begin{enumerate}
		\item Déterminer $f'(x)$ pour tout réel $x$ appartenant à $\intervOO{0}{+\infty}$.
		\item Étudier le signe de $f'(x)$ sur l'intervalle $\intervOO{0}{+\infty}$.
		
		En déduire le tableau de variations de $f$.
	\end{enumerate}
	\item Montrer que l'équation $f(x) = 0$ admet une unique solution dans l'intervalle $\intervFF{4}{5}$.
	\item On admet que, pour tout $x$ de $\intervOO{0}{+\infty}$, on a : \[f''(x) = \dfrac{2x^2 - 4}{x^2}.\]
	\begin{enumerate}
		\item Étudier la convexité de la fonction $f$ sur $\intervOO{0}{+\infty}$. 
		
		On précisera les valeurs exactes des coordonnées des éventuels points d'inflexion de $\mathcal{C}_f$. 
		\item On note A le point de coordonnées $\left(\sqrt 2;f\left(\sqrt 2~\right)\right)$.
		
		Soit $t$ un réel strictement positif tel que $t \ne \sqrt 2$. Soit $M$ le point de coordonnées $(t;f(t))$.
		
		En utilisant la question 4.\pta{a}, indiquer, selon la valeur de $t$, les positions relatives du segment $[AM]$ et de la courbe $\mathcal{C}_f$.
	\end{enumerate}
\end{enumerate}

\pagebreak

\section*{Exercice 2 [Thèmes : suites, fonctions, fonction exponentielle]\dotfill(7 points)} %exo2

\medskip

On considère la fonction $f$ définie sur $\R$ par \[f(x) = x^3\e^x.\]
%
On admet que la fonction $f$ est dérivable sur $\R$ et on note $f'$ sa fonction dérivée.

\begin{enumerate}
	\item On définit la suite $\left(u_n\right)$ par $u_0 = - 1$ et, pour tout entier naturel $n$, $u_{n+1} = f\left(u_n\right)$.
	\begin{enumerate}
		\item Calculer $u_1$  puis $u_2$.
		
		On donnera les valeurs exactes, puis les valeurs approchées à $10^{-3}$.
		\item On considère la fonction \texttt{fonc}, écrite en langage \textsf{Python} ci-dessous.
		
		\smallskip
		
		On rappelle qu'en langage \textsf{Python}, \texttt{i in range(n)} signifie que \texttt{i} varie de \texttt{0} à \texttt{n-1}.
		%
\begin{CodePythonLstAlt}*[Largeur=8cm]{center}
def fonc(n) :
	u = -1
	for i in range(n) :
		u = u**3*exp(u)
	return u
\end{CodePythonLstAlt}
		%
		Déterminer, sans justifier, la valeur renvoyée par \texttt{fonc(2)} arrondie à $10^{-3}$.
	\end{enumerate}
	\item 
	\begin{enumerate}
		\item Démontrer que, pour tout $x$ réel, on a $f'(x) = x^2\e^x(x + 3)$.
		\item Justifier que le tableau de variations de $f$ sur $\R$ est celui représenté ci-dessous :
		
		\begin{center}
			\begin{tikzpicture}
				\tkzTabInit{$x$/1,$f$/2}{$-\infty$,$-3$,$+\infty$}
				\tkzTabVar{+/$0$,-/$- 27\e^{-3}$,+/$+\infty$}
			\end{tikzpicture}
		\end{center}
		\item Démontrer, par récurrence, que pour tout entier naturel $n$, on a : \[- 1 \leqslant u_n \leqslant u_{n+1} \leqslant 0.\]
		\item En déduire que la suite $\left(u_n\right)$ est convergente.
		\item On note $\ell$ la limite de la suite $\left(u_n\right)$.
		
		On rappelle que $\ell$ est solution de l'équation $f(x) = x$.
		
		Déterminer $\ell$. Pour cela, on admettra que l'équation $x^2\e^x - 1 = 0$ possède une seule solution dans $\R$ et que celle-ci est strictement supérieure à $\dfrac12$).
	\end{enumerate}
\end{enumerate}

\pagebreak

\section*{Exercice 3 [Thèmes : géométrie dans l'espace]\dotfill(7 points)} %exo3

\medskip

Une maison est constituée d'un parallélépipède rectangle $ABCDEFGH$ surmonté d'un prisme $EFIHGJ$ dont une base est le triangle $EIF$ isocèle en $I$.

Cette maison est représentée ci-dessous.

\begin{center}
	\begin{tikzpicture}[line join=bevel]
		\draw[semithick] (0.4,3.4)--(0.4,1.5)--(1.9,0.4)--(1.9,2.3)--cycle;%HDAE
		\draw[semithick] (1.9,0.4)--(7.9,0.8)--(7.9,2.7)--(1.9,2.3);%ABFE
		\draw[semithick,dashed] (0.4,1.5)--(6.4,1.9)--(6.4,3.8)--(0.4,3.4);%DCGH
		\draw[semithick,dashed] (7.9,0.8)--(6.4,1.9);%BC
		\draw[semithick] (7.9,2.7)--(6.4,3.8)--(3.4,5.5)--(4.9,4.4)--cycle;%FGJI
		\draw[semithick] (4.9,4.4)--(1.9,2.3);%IE
		\draw[semithick] (3.4,5.5)--(0.4,3.4);%JH
		\draw[very thick,->,>=latex] (1.9,0.4)--(3.9,0.533);
		\draw[very thick,->,>=latex] (1.9,0.4)--(1.15,0.95);
		\draw[very thick,->,>=latex] (1.9,0.4)--(1.9,2.3);
		\draw[semithick] (11.6,0.3)--(7.9,1.2);
		\draw[semithick,dashed] (7.9,1.2)--(7.15,1.35);
		%points
		\draw (1.9,0.4) node[below] {A} ;
		\draw (7.9,0.8) node[below right] {B} ;
		\draw (6.4,2.1) node[right] {C} ;
		\draw (0.4,1.5) node[left] {D} ;
		\draw (1.9,2.3) node[above] {E} ;
		\draw (7.9,2.7) node[right] {F} ;
		\draw (6.4,3.8) node[above right] {G} ;
		\draw (0.4,3.4) node[above left] {H} ;
		\draw (4.9,4.4) node[below] {I} ;
		\draw (3.4,5.5) node[above] {J} ;
		\filldraw (11.6,0.3) circle[radius=2pt] node[right] {R} ;
		\draw (1.7,0.6) node[below left] {$\vect{\jmath}$} ;
		\draw (2.9,0.45) node[below right] {$\vect{\imath}$} ;
		\draw (1.9,1.35) node[right] {$\vect{k}$} ;
	\end{tikzpicture}
\end{center}

On a $AB=3$, $AD=2$ et  $AE=1$. On définit les vecteurs $\vect{\imath}= \dfrac13\vect{\text{AB}}$, $\vect{\jmath}= \dfrac12\vect{\text{AD}}$ et $\vect{k} = \vect{\text{AE}}$.

\smallskip

On munit ainsi l'espace du repère orthonormé $\left(A;\vect{\imath},\,\vect{\jmath},\,\vect{k}\right)$.

\begin{enumerate}
	\item Donner les coordonnées du point $G$.
	\item Le vecteur $\vect{n}$ de coordonnées $(2;0;-3)$ est vecteur normal au plan $(EHI)$.
	
	Déterminer une équation cartésienne du plan $(EHI)$.
	\item Déterminer les coordonnées du point $I$.
	\item Déterminer une mesure au degré près de l'angle $\widehat{EIF}$.
	\item Afin de raccorder la maison au réseau électrique, on souhaite creuser une tranchée rectiligne depuis un relais électrique situé en contrebas de la maison.
	
	Le relais est représenté par le point R de coordonnées $(6;-3;-1)$.
	
	La tranchée est assimilée à un segment d'une droite $\Delta$ passant par $R$ et dirigée par le vecteur $\vect{n}$ de coordonnées $(-3;4;1)$. On souhaite vérifier que la tranchée atteindra la maison au niveau de l'arête $[BC]$.
	\begin{enumerate}
		\item Donner une représentation paramétrique de la droite $\Delta$.
		\item On admet qu'une équation du plan $(BFG)$ est $x = 3$.
		
		Soit $K$ le point d'intersection de la droite $\Delta$ avec le plan $(BFG)$.
		
		Déterminer les coordonnées du point $K$.
		\item Le point K appartient-il bien à l'arête $[BC]$ ?
	\end{enumerate}
\end{enumerate}

\pagebreak

\section*{Exercice 4 [Thèmes : probabilités]\dotfill(7 points)} %exo4

\medskip

\emph{Cet exercice est un questionnaire à choix multiples.\\
	Pour chacune des questions suivantes, une seule des quatre réponses proposées est exacte.\\
	Une réponse fausse, une réponse multiple ou l'absence de réponse à une question ne rapporte ni n'enlève de point.\\
	Pour répondre, indiquer sur la copie le numéro de la question et la lettre de la réponse choisie. Aucune justification n'est demandée.}

\medskip

\begin{wrapstuff}[r,leftsep=1.5em,rightsep=1em,top=1]
	\def\ArbreDeuxDeux{
		$E_0$/\num{0.4}/above,$R_0$/\ldots/above,$R_1$/\num{0.01}/below,%
		$E_1$/\ldots/below,$R_1$/\num{0.02}/above,$R_1$/\ldots/below
	}
	\ArbreProbasTikz[EspaceNiveau=2.25,EspaceFeuille=1]{\ArbreDeuxDeux}
\end{wrapstuff}
On considère un système de communication binaire transmettant des $0$ et des $1$.

Chaque $0$ ou $1$ est appelé bit.

En raison d'interférences, il peut y avoir des erreurs de transmission:

un $0$ peut être reçu comme un $1$ et, de même, un $1$ peut être reçu comme un $0$.

Pour un bit choisi au hasard dans le message, on note les évènements :

\begin{itemize}
	\item $E_0$ : \og le bit envoyé est un $0$ \fg{} ;
	\item $E_1$ : \og le bit envoyé est un 1 \fg{} ;
	\item $R_0$ : \og le bit reçu est un $0$\fg{} 
	\item $R_1$ : \og le bit reçu est un $1$ \fg.
\end{itemize}

On sait que $p\left(E_0\right) = 0,4$ ; $p_{R_0}\left(R_1\right) = 0,01$ et $p_{R_1}\left(R_0\right) = 0,02$.

\smallskip

On rappelle que la probabilité conditionnelle de $A$ sachant $B$ est notée $p_B(A)$.

\smallskip

On peut ainsi représenter la situation par l'arbre de probabilités ci-contre.

\begin{enumerate}
	\item La probabilité que le bit envoyé soit un $0$ et que le bit reçu soit un $0$ est égale à :
	
	\smallskip
	
	\begin{tblr}{width=\linewidth,colspec={*{4}{X[m,l]}}}
		\pta{a}~~$0,99$ &\pta{b}~~$0,396$ &\pta{c}~~$0,01$ &\pta{d}~~$0,4$ \\
	\end{tblr}
	\item La probabilité  $p\left(R_0\right)$ est égale à :
	
	\smallskip
	
	\begin{tblr}{width=\linewidth,colspec={*{4}{X[m,l]}}}
		\pta{a}~~$0,99$ &\pta{b}~~$0,02$ &\pta{c}~~$0,408$ &\pta{d}~~$0,931$ \\
	\end{tblr}
	\item Une valeur, approchée au millième, de la probabilité $p_{R_1}\left(R_0\right)$ est :
	
	\smallskip
	
	\begin{tblr}{width=\linewidth,colspec={*{4}{X[m,l]}}}
		\pta{a}~~$0,004$ &\pta{b}~~$0,001$ &\pta{c}~~$0,007$ &\pta{d}~~$0,010$ \\
	\end{tblr}
	\item La probabilité de l'évènement \og il y a une erreur de transmission \fg{} est égale à :
	
	\smallskip
	
	\begin{tblr}{width=\linewidth,colspec={*{4}{X[m,l]}}}
		\pta{a}~~$0,03$ &\pta{b}~~$0,016$ &\pta{c}~~$0,16$ &\pta{d}~~$0,015$ \\
	\end{tblr}
\end{enumerate}

\end{document}