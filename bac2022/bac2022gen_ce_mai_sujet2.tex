% !TeX TXS-program:compile = txs:///pdflatex

\documentclass[french,a4paper,11pt]{article}
\usepackage[margin=2cm,includeheadfoot]{geometry}
\usepackage{ProfLycee}
\RequirePackage[upright]{fourier}
\usepackage{amsmath,amssymb,amstext}
\usepackage[scaled=0.875]{helvet}
\renewcommand\ttdefault{lmtt}
\usepackage{decimalcomma}
\usepackage{esvect}
\usepackage{tabularx}
\usepackage{wrapstuff}
\usepackage{logoetalab}

\newcommand{\session}{2022}
\newcommand{\annee}{2022}
\newcommand{\serie}{Gé.}
\newcommand{\lieu}{Centres Étrangers}
\newcommand{\jour}{12}
\newcommand{\mois}{Mai}
\newcommand{\numsujet}{2}
\newcommand{\nomfichier}{[BAC \serie{} \annee{}] \lieu{} (\mois{} \annee)}

\title{\nomfichier}
\usepackage{hyperref}
\usepackage{lastpage}
\usepackage{enumitem}
\usepackage{ibrackets}
\usepackage{fancyhdr}
\usepackage{babel}

\hypersetup{pdfauthor={Pierquet},pdftitle={\nomfichier},allbordercolors=white,pdfborder=0 0 0,pdfstartview=FitH}
\lhead{\scriptsize\sffamily BAC \serie{} \session}
\chead{\scriptsize\sffamily \lieu{} - Sujet \numsujet}
\rhead{\scriptsize\sffamily \jour{} \mois{} \annee{}}
\lfoot{\scriptsize\sffamily \affloetalab[Legende,TexteLegende={Licence Etalab 2.0}]}
\cfoot{\scriptsize\sffamily 22-MATJ2G11}
\rfoot{\scriptsize\sffamily - \thepage{}/{}\pageref{LastPage} -}
\setlength{\parindent}{0pt}
%divers
\DeclareMathSymbol{;}\mathbin{operators}{'73}
\newcommand\Rij{\left(O;\vect{\imath},\,\vect{\jmath}\right)}
\newcommand\Rijk{\left(O;\vect{\imath},\,\vect{\jmath},\,\vect{k}\right)}
\newcommand\R{\mathbb{R}}
\newcommand\e{\text{e}}
\newcommand\pta[1]{(#1)}
\newcommand\suiten[1][]{\left(#1_n\right)}
\newcommand\vect[1]{\vv{#1}}
\usepackage{forest}
\forestset{
	fleche/.style = {edge={thick}},
	aproba/.style = {edge label = {node[above=6pt,pos=.5,fill=white] {$#1$}}},%poids au-dessus
	bproba/.style = {edge label = {node[below=6pt,pos=.5,fill=white] {$#1$}}},%poids en-dessous
}
\usepackage{pas-tableur}

\begin{document}

\pagestyle{fancy}

{\hfill\Huge \faGraduationCap\hfill~}

\part*{\lieu{}, Bac \serie{}, \jour{} \mois{} \annee, sujet n°\numsujet}

\vspace{0.25cm}

\hrulefill~\textsf{Le candidat traite trois des \textbf{4 exercices} proposés.}~\hrulefill

\vspace{0.25cm}

\section*{Exercice 1 [Thème : Fonction exponentielle]\dotfill(7 points)}

\medskip

\textit{Cet exercice est un questionnaire à choix multiples. Pour chacune des questions suivantes, une seule des quatre réponses proposées est exacte. Les six questions sont indépendantes.\\
	Une réponse incorrecte, une réponse multiple ou l’absence de réponse à une question ne rapporte ni n’enlève de point. Pour répondre, indiquer sur la copie le numéro de la question et la lettre de la réponse choisie.\\
	Aucune justification n’est demandée.}

\begin{enumerate}
	\item Soit $f$ la fonction définie sur $\R$ par $f(x)=\dfrac{x}{\e^x}$.
	
	On suppose que $f$ est dérivable sur $\R$ et on note $f'$ sa fonction dérivée.
	
	\begin{tblr}{width=\linewidth,colspec={X[l]X[l]}}
		(a)~~$f'(x)=\e^{-x}$&(b)~~$f'(x)=x\,\e^{-x}$\\
		(c)~~$f'(x)=(1-x)\e^{-x}$&(d)~~$f'(x)=(1+x)\e^{-x}$
	\end{tblr}
	%
	\item Soit $f$ une fonction deux fois dérivable sur l'intervalle $[-3;1]$. On donne ci-dessous la représentation graphique de sa fonction dérivée seconde $f''$.
	
	\begin{center}
		\begin{tikzpicture}[x=2cm,y=1cm,xmin=-3.5,xmax=1.5,xgrille=1,xgrilles=0.5,ymin=-3.5,ymax=3.5,ygrille=1,ygrilles=0.5]
			\GrilleTikz[Affs=false] \AxesTikz[ElargirOx=0/0,ElargirOy=0/0]
			\AxexTikz{-3,-2,-1,1} \AxeyTikz{-3,-2,-1,1,2,3}
			\draw (0,0) node[below left=2pt] {0} ;
			\clip (\xmin,\ymin) rectangle (\xmax,\ymax) ;
			\draw[very thick,red,domain=-3:1,samples=250] plot (\x,{0.9743*\x*\x*\x + 2.9229*\x*\x - 0.9743*\x - 2.9229}) ;
		\end{tikzpicture}
	\end{center}
	
	On peut alors affirmer que :
	
	\begin{tblr}{width=\linewidth,colspec={X[l]X[l]}}
		(a)~~La fonction $f$ est convexe sur l'intervalle $[-1;1]$&(b)~~La fonction $f$ est concave sur l'intervalle $[-2;0]$\\
		(c)~~La fonction $f'$ est décroissante sur $[-2;0]$&(d)~~La fonction $f'$ admet un maximum en $x=-1$
	\end{tblr}
	%
	\item On considère la fonction $f$ définie sur $\R$ par $f(x)=x^3\,\e^{-x^2}$.
	
	Une primitive $F$ de la fonction $f$ est définie sur $\R$ par :
	
	\begin{tblr}{width=\linewidth,colspec={X[l]X[l]}}
		(a)~~$F(x)=-\frac16 (x^3+1)\e^{-x^2}$&(b)~~$F(x)=-\frac14 x^4\,\e^{-x^2}$\\
		(c)~~$F(x)=-\frac12 (x^2+1)\e^{-x^2}$&(d)~~$F(x)=x^2(3-2x^2)\e^{-x^2}$
	\end{tblr}
	%
	\item Que vaut $\displaystyle\lim_{x \to +\infty} \dfrac{\e^x+1}{\e^x-1}$ ?
	
	\begin{tblr}{width=\linewidth,colspec={X[l]X[l]}}
		(a)~~$-1$&(b)~~$1$\\
		(c)~~$+\infty$&(d)~~Elle n'existe pas
	\end{tblr}
	%
	\item On considère la fonction $f$ définie sur $\R$ par $f(x)=\e^{2x+1}$.
	
	La seule primitive $F$ sur $\R$ de la fonction $f$ telle que $F(0)=1$ est la fonction :
	
	\begin{tblr}{width=\linewidth,colspec={X[l]X[l]}}
		(a)~~$x \mapsto 2\,\e^{2x+1}-2\e+1$&(b)~~$x \mapsto \e^{2x+1}-\e$\\
		(c)~~$x \mapsto \dfrac12 \e^{2x+1} - \dfrac12 \e+1$&(d)~~$x \mapsto \e^{x^2+x}$
	\end{tblr}
	%
	\item Dans un repère, on a tracé ci-dessous la courbe représentative d'une fonction $f$ définie et deux fois dérivable sur $[-2;4]$.
	
	\begin{center}
		\begin{tikzpicture}[x=0.75cm,y=0.75cm,xmin=-2.5,xmax=5.5,xgrille=1,xgrilles=0.5,ymin=-2,ymax=3,ygrille=1,ygrilles=0.5]
			\tikzset{courbe/.style={line width=1pt,red,samples=250}}
			%grilles & axes
			\GrilleTikz \AxesTikz[ElargirOx=0/0,ElargirOy=0/0]
			\AxexTikz[AffGrad=false]{-2,-1,...,4}
			\AxeyTikz[AffGrad=false]{-2,-1,...,2}
			\draw (0,0) node[below left=2pt,font=\scriptsize] {0} ;
			\draw (1,0) node[below=4pt,font=\scriptsize] {1} ;
			\draw (0,1) node[left=4pt,font=\scriptsize] {1} ;
			\clip (\xmin,\ymin) rectangle (\xmax,\ymax) ; %on restreint les fonctions à la fenêtre
			\draw[very thick,blue,domain=\xmin:\xmax,samples=250] plot (\x,{1/6*\x*\x*\x-1/2*\x*\x+1}) ;
		\end{tikzpicture}
	\end{center}
	
	Parmi les courbes suivantes, laquelle représente la fonction $f''$, dérivée seconde de $f$ ?
	
	\begin{tblr}{width=\linewidth,colspec={X[l]X[l]}}
		(a)~~%
		{\begin{tikzpicture}[x=0.75cm,y=0.75cm,xmin=-2,xmax=4,xgrille=1,xgrilles=0.5,ymin=-3,ymax=3,ygrille=1,ygrilles=0.5,baseline=(current bounding box.north)]
				\tikzset{courbe/.style={line width=1pt,red,samples=250}}
				%grilles & axes
				\GrilleTikz \AxesTikz[ElargirOx=0/0,ElargirOy=0/0]
				\AxexTikz[AffGrad=false]{-2,-1,...,4}
				\AxeyTikz[AffGrad=false]{-2,-1,...,2}
				\draw (0,0) node[below left=2pt,font=\scriptsize] {0} ;
				\draw (1,0) node[below=4pt,font=\scriptsize] {1} ;
				\draw (0,1) node[left=4pt,font=\scriptsize] {1} ;
				\clip (\xmin,\ymin) rectangle (\xmax,\ymax) ; %on restreint les fonctions à la fenêtre
				\draw[very thick,red,domain=\xmin:\xmax,samples=2] plot (\x,{\x-1}) ;
		\end{tikzpicture}}%
		&(b)~~%
		{\begin{tikzpicture}[x=0.75cm,y=0.75cm,xmin=-2,xmax=4,xgrille=1,xgrilles=0.5,ymin=-3,ymax=3,ygrille=1,ygrilles=0.5,baseline=(current bounding box.north)]
				\tikzset{courbe/.style={line width=1pt,red,samples=250}}
				%grilles & axes
				\GrilleTikz \AxesTikz[ElargirOx=0/0,ElargirOy=0/0]
				\AxexTikz[AffGrad=false]{-2,-1,...,4}
				\AxeyTikz[AffGrad=false]{-2,-1,...,2}
				\draw (0,0) node[below left=2pt,font=\scriptsize] {0} ;
				\draw (1,0) node[below=4pt,font=\scriptsize] {1} ;
				\draw (0,1) node[left=4pt,font=\scriptsize] {1} ;
				\clip (\xmin,\ymin) rectangle (\xmax,\ymax) ; %on restreint les fonctions à la fenêtre
				\draw[very thick,red,domain=\xmin:\xmax,samples=2] plot (\x,{1-\x}) ;
		\end{tikzpicture}}%
		\\
		(c)~~%
		{\begin{tikzpicture}[x=0.75cm,y=0.75cm,xmin=-2,xmax=4,xgrille=1,xgrilles=0.5,ymin=-3,ymax=3,ygrille=1,ygrilles=0.5,baseline=(current bounding box.north)]
				\tikzset{courbe/.style={line width=1pt,red,samples=250}}
				%grilles & axes
				\GrilleTikz \AxesTikz[ElargirOx=0/0,ElargirOy=0/0]
				\AxexTikz[AffGrad=false]{-2,-1,...,4}
				\AxeyTikz[AffGrad=false]{-2,-1,...,2}
				\draw (0,0) node[below left=2pt,font=\scriptsize] {0} ;
				\draw (1,0) node[below=4pt,font=\scriptsize] {1} ;
				\draw (0,1) node[left=4pt,font=\scriptsize] {1} ;
				\clip (\xmin,\ymin) rectangle (\xmax,\ymax) ; %on restreint les fonctions à la fenêtre
				\draw[very thick,red,domain=\xmin:\xmax,samples=250] plot (\x,{1/3*\x*(\x-2)}) ;
		\end{tikzpicture}}%
		&(d)~~%
		{\begin{tikzpicture}[x=0.75cm,y=0.75cm,xmin=-2,xmax=4,xgrille=1,xgrilles=0.5,ymin=-3,ymax=3,ygrille=1,ygrilles=0.5,baseline=(current bounding box.north)]
				\tikzset{courbe/.style={line width=1pt,red,samples=250}}
				%grilles & axes
				\GrilleTikz \AxesTikz[ElargirOx=0/0,ElargirOy=0/0]
				\AxexTikz[AffGrad=false]{-2,-1,...,4}
				\AxeyTikz[AffGrad=false]{-2,-1,...,2}
				\draw (0,0) node[below left=2pt,font=\scriptsize] {0} ;
				\draw (1,0) node[below=4pt,font=\scriptsize] {1} ;
				\draw (0,1) node[left=4pt,font=\scriptsize] {1} ;
				\clip (\xmin,\ymin) rectangle (\xmax,\ymax) ; %on restreint les fonctions à la fenêtre
				\draw[very thick,red,domain=\xmin:\xmax,samples=250] plot (\x,{0.5*(\x-1)*(\x-1)}) ;
		\end{tikzpicture}}%
	\end{tblr}
\end{enumerate}

\pagebreak

\section*{Exercice 2 [Thèmes : Fonction logarithme et suite]\dotfill(7 points)}

\medskip

Soit $f$ la fonction définie sur l'intervalle $]0;+\infty[$ par \[f(x) = x\ln (x) + 1.\]
%
On note $\mathcal{C}_f$ sa courbe représentative dans un repère du plan.

\begin{enumerate}
	\item Déterminer la limite de la fonction $f$ en $0$ ainsi que sa limite en $+\infty$.
	\item 
	\begin{enumerate}
		\item On admet que $f$ est dérivable sur $]0;+\infty[$ et on notera $f'$ sa fonction dérivée.
		
		Montrer que pour tout réel $x$ strictement positif : \[f'(x) = 1 + \ln (x).\]
		\item En déduire le tableau de variation de la fonction $f$ sur $]0;+\infty[$. On y fera figurer la valeur exacte de l'extremum de $f$ sur $]0;+\infty[$ et les limites.
		\item Justifier que pour tout $x \in  ]0;1[$, $f(x) \in  ]0;1[$.
	\end{enumerate}	
	\item 
	\begin{enumerate}
		\item Déterminer une équation de la tangente $(T)$ à la courbe $\mathcal{C}_f$ au point d'abscisse 1.
		\item Étudier la convexité de la fonction $f$ sur $]0;,+\infty[$.
		\item En déduire que pour tout réel $x$ strictement positif : \[f(x) \geqslant x.\]
	\end{enumerate}
	\item On définit la suite $\left(u_n\right)$ par son premier terme $u_0$ élément de l'intervalle $]0;1[$ et pour tout entier naturel $n$ : \[u_{n+1} = f\left(u_n\right).\]
	\begin{enumerate}
		\item Démontrer par récurrence que pour tout entier naturel $n$, on a : $0 < u_n < 1$.
		\item Déduire de la question 3.(c) la croissance de la suite $\left(u_n\right)$.
		\item En déduire que la suite $\left(u_n\right)$ est convergente.
	\end{enumerate}
\end{enumerate}

\vspace{0.5cm}

\section*{Exercice 3 [Thème : Géométrie dans l'espace]\dotfill(7 points)}

\medskip

L'espace est muni d'un repère orthonormé $\Rijk$.

On considère les points \[A(3;-2;2) \text{, } B(6;1;5) \text{, } C(6;-2;-1) \text{ et } D(0;4;-1).\]%
\emph{On rappelle que le volume d'un tétraèdre est donné par la formule :}\[V = \dfrac13 \mathcal{A} \times h\]%
\emph{où $\mathcal{A}$ est l'aire de la base et $h$ la hauteur correspondante.}

\begin{enumerate}
	\item Démontrer que les points A, B, C et D ne sont pas coplanaires.
	\item 
	\begin{enumerate}
		\item Montrer que le triangle ABC est rectangle.
		\item Montrer que la droite (AD) est perpendiculaire au plan (ABC).
		\item En déduire le volume du tétraèdre ABCD.
	\end{enumerate}	
	\item On considère le point $H(5;0;1)$.
	\begin{enumerate}
		\item Montrer qu'il existe des réels $\alpha$ et $\beta$ tels que $\vect{\text{BH}} = \alpha \vect{\text{BC}} + \beta \vect{\text{BD}}$.
		\item Démontrer que H est le projeté orthogonal du point A sur le plan (BCD). 
		\item En déduire la distance du point A au plan (BCD).
	\end{enumerate}	
	\item Déduire des questions précédentes l'aire du triangle BCD.
\end{enumerate}

\pagebreak

\section*{Exercice 4 [Thème : Probabilités]\dotfill(7 points)}

\medskip

Une urne contient des jetons blancs et noirs tous indiscernables au toucher.

Une partie consiste à prélever au hasard successivement et avec remise deux jetons de cette urne.

On établit la règle de jeu suivante:

\begin{itemize}
	\item un joueur perd 9 euros si les deux jetons tirés sont de couleur blanche ;
	\item un joueur perd 1 euro si les deux jetons tirés sont de couleur noire ;
	\item un joueur gagne 5 euros si les deux jetons tirés sont de couleurs différentes.
\end{itemize}

\begin{enumerate}
	\item On considère que l'urne contient 2 jetons noirs et 3 jetons blancs.
	\begin{enumerate}
		\item Modéliser la situation à l'aide d'un arbre pondéré.
		\item Calculer la probabilité de perdre 9\,€ sur une partie.
	\end{enumerate}	
	\item On considère maintenant que l'urne contient 3 jetons blancs et au moins deux jetons noirs mais on ne connait pas le nombre exact de jetons noirs. On appellera $N$ le nombre de jetons noirs.
	\begin{enumerate}
		\item Soit $X$ la variable aléatoire donnant le gain du jeu pour une partie.
		
		Déterminer la loi de probabilité de cette variable aléatoire.
		\item Résoudre l'inéquation pour $x$ réel : \[-x^2  + 30x - 81 > 0.\]
		\item En utilisant le résultat de la question précédente, déterminer le nombre de jetons noirs que l'urne doit contenir afin que ce jeu soit favorable au joueur.
		\item Combien de jetons noirs le joueur doit-il demander afin d'obtenir un gain moyen maximal ?
	\end{enumerate}
	\item On observe $10$ joueurs qui tentent leur chance en effectuant une partie de ce jeu, indépendamment les uns des autres. On suppose que 7 jetons noirs ont été placés dans l'urne (avec 3 jetons blancs).
	
	Quelle est la probabilité d'avoir au moins $1$ joueur gagnant $5$ euros?
\end{enumerate}

\end{document}