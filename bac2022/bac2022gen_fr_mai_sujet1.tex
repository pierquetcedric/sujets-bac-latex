% !TeX TXS-program:compile = txs:///pdflatex

\documentclass[french,a4paper,11pt]{article}
\usepackage[margin=2cm,includeheadfoot]{geometry}
\usepackage{ProfLycee}
\RequirePackage[upright]{fourier}
\usepackage{amsmath,amssymb,amstext}
\usepackage[scaled=0.875]{helvet}
\renewcommand\ttdefault{lmtt}
\usepackage{decimalcomma}
\usepackage{esvect}
\usepackage{wrapstuff}
\usepackage{logoetalab}

\newcommand{\session}{2022}
\newcommand{\annee}{2022}
\newcommand{\serie}{Gé.}
\newcommand{\lieu}{Métropole}
\newcommand{\jour}{11}
\newcommand{\mois}{Mai}
\newcommand{\numsujet}{1}
\newcommand{\nomfichier}{[BAC \serie{} \annee{}] \lieu{} (\mois{} \annee)}

\title{\nomfichier}
\usepackage{hyperref}
\usepackage{lastpage}
\usepackage{enumitem}
\usepackage{ibrackets}
\usepackage{fancyhdr}
\usepackage{babel}

\hypersetup{pdfauthor={Pierquet},pdftitle={\nomfichier},allbordercolors=white,pdfborder=0 0 0,pdfstartview=FitH}
\lhead{\scriptsize\sffamily BAC \serie{} \session}
\chead{\scriptsize\sffamily \lieu{} - Sujet \numsujet}
\rhead{\scriptsize\sffamily \jour{} \mois{} \annee{}}
\lfoot{\scriptsize\sffamily \affloetalab[Legende,TexteLegende={Licence Etalab 2.0}]}
\cfoot{\scriptsize\sffamily 22-MATJ1ME1}
\rfoot{\scriptsize\sffamily - \thepage{}/{}\pageref{LastPage} -}
\setlength{\parindent}{0pt}
%divers
\DeclareMathSymbol{;}\mathbin{operators}{'73}
\newcommand\Rijk{\left(O;\vect{\imath},\,\vect{\jmath},\,\vect{k}\right)}
\newcommand\R{\mathbb{R}}
\newcommand\e{\text{e}}
\newcommand\suiten[1][]{\left(#1_n\right)}
\newcommand\vect[1]{\vv{#1}}
\usepackage{forest}
\forestset{
	fleche/.style = {edge={thick}},
	aproba/.style = {edge label = {node[above=6pt,pos=.5,fill=white] {$#1$}}},%poids au-dessus
	bproba/.style = {edge label = {node[below=6pt,pos=.5,fill=white] {$#1$}}},%poids en-dessous
}

\begin{document}

\pagestyle{fancy}

{\hfill\Huge \faGraduationCap\hfill~}

\part*{\lieu{}, Bac \serie{}, \jour{} \mois{} \annee, sujet n°\numsujet}

\vspace{0.25cm}

\hrulefill~\textsf{Le candidat traite trois des \textbf{4 exercices} proposés.}~\hrulefill

\vspace{0.25cm}

\section*{Exercice 1 [Thèmes : fonction exponentielle, suites]\dotfill(7 points)}

\medskip

Dans le cadre d’un essai clinique, on envisage deux protocoles de traitement d’une maladie.

L’objectif de cet exercice est d’étudier, pour ces deux protocoles, l’évolution de la quantité de médicament présente dans le sang d’un patient en fonction du temps.

\smallskip

\textit{Les parties A et B sont indépendantes.}

\medskip

\textbf{Partie A : Étude du premier protocole}

\medskip

Le premier protocole consiste à faire absorber un médicament, sous forme de comprimé, au patient.

On modélise la quantité de médicament présente dans le sang du patient, exprimée en mg, par la fonction $f$ définie sur l’intervalle $[0;10]$ par $f(t)=3t\,\e^{-0,5t+1}$, où $t$ désigne le temps, exprimé en heure, écoulé depuis la prise du comprimé. 

\begin{enumerate}
	\item 
	\begin{enumerate}
		\item On admet que la fonction $f$ est dérivable sur l’intervalle $[0;10]$ et on note $f'$ sa fonction dérivée.
		
		Montrer que, pour tout nombre réel $t$ de $[0;10]$, on a : $f'(t)=3(-0,5t+1)\,\e^{-0,5t+1}$.
		\item En déduire le tableau de variations de la fonction $f$ sur l’intervalle $[0;10]$.
		\item Selon cette modélisation, au bout de combien de temps la quantité de médicament présente dans le sang du patient sera-t-elle maximale ? Quelle est alors cette quantité maximale ?
	\end{enumerate}
	\item 
	\begin{enumerate}[series=mylist]
		\item Montrer que l’équation $f(t)=5$ admet une unique solution sur l’intervalle $[0;2]$, notée $\alpha$, dont on donnera une valeur approchée à $10^{-2}$ près.
	\end{enumerate}
\end{enumerate}

On admet que l’équation $f(t)=5$ admet une unique solution sur l’intervalle $[2;10]$, notée $\beta$, et qu’une valeur approchée de $\beta$ à $10^{-2}$ près est $3,46$.

\begin{enumerate}
	\item[]
	\begin{enumerate}[resume=mylist]
		\item On considère que ce traitement est efficace lorsque la quantité de médicament présente dans le sang du patient est supérieure ou égale à 5 mg.
		
		Déterminer, à la minute près, la durée d’efficacité du médicament dans le cas de ce protocole. 
	\end{enumerate}
\end{enumerate}

\textbf{Partie B : Étude du deuxième protocole}

\medskip

Le deuxième protocole consiste à injecter initialement au patient, par piqûre intraveineuse, une dose de 2 mg de médicament puis à réinjecter toutes les heures une dose de 1,8 mg. 

On suppose que le médicament se diffuse instantanément dans le sang et qu’il est ensuite progressivement éliminé.

On estime que lorsqu'une heure s’est écoulée après une injection, la quantité de médicament dans le sang a diminué de 30\,\% par rapport à la quantité présente immédiatement après cette injection.

On modélise cette situation à l’aide de la suite $\suiten$ où, pour tout entier naturel $n$, $u_n$ désigne la quantité de médicament, exprimée en mg, présente dans le sang du patient immédiatement après l’injection de la $n$-ème heure. On a donc $u_0 = 2$.

\begin{enumerate}
	\item Calculer, selon cette modélisation, la quantité $u_1$ de médicament (en mg) présente dans le sang du patient immédiatement après l’injection de la première heure.
	\item Justifier que, pour tout entier naturel $n$, on a : $u_{n+1} = 0,7u_n + 1,8$.
	\item 
	\begin{enumerate}
		\item Montrer par récurrence que, pour tout entier naturel $n$, on a : $u_n \leqslant u_{n+1} < 6$.
		\item En déduire que la suite $\suiten$ est convergente. On note $\ell$ sa limite. 
		\item Déterminer la valeur de $\ell$. Interpréter cette valeur dans le contexte de l’exercice. 
	\end{enumerate}
	\item On considère la suite $\suiten[v]$ définie, pour tout entier naturel $n$, par $v_n=6-u_n$.
	\begin{enumerate}
		\item Montrer que la suite $\suiten[v]$ est une suite géométrique de raison $0,7$ dont on précisera le premier
		terme. 
		\item Déterminer l’expression de $\suiten[v]$ en fonction de $n$, puis de $u_n$ en fonction de $n$.
		\item Avec ce protocole, on arrête les injections lorsque la quantité de médicament présente dans le sang du patient est supérieure ou égale à $5,5$ mg.
		
		Déterminer, en détaillant les calculs, le nombre d’injections réalisées en appliquant ce protocole.
	\end{enumerate}
\end{enumerate}

\vspace{0.5cm}

\section*{Exercice 2 [Thème : géométrie dans l’espace]\dotfill(7 points)}

\medskip

Dans l’espace rapporté à un repère orthonormé $\Rijk$, on considère :

\begin{itemize}
	\item le point $A$ de coordonnées $(-1;1;3)$ ;
	\item la droite $\mathcal{D}$ dont une représentation paramétrique est : $\begin{dcases} x=1+2t\\y=2-t\\z=2+2t\end{dcases}$, $t \in \mathbb{R}$.
\end{itemize}

On admet que le point $A$ n’appartient pas à la droite $\mathcal{D}$.

\begin{enumerate}
	\item 
	\begin{enumerate}
		\item Donner les coordonnées d’un vecteur directeur $\vect{u}$ de la droite $\mathcal{D}$.
		\item Montrer que le point $B(-1\,;\,3\,;\,0)$ appartient à la droite $\mathcal{D}$.
		\item Calculer le produit scalaire $\vect{AB}\cdot\vect{u}$.
	\end{enumerate}
	\item On note $\mathcal{P}$ le plan passant par le point $A$ et orthogonal à la droite $\mathcal{D}$, et on appelle $H$ le point d’intersection du plan $\mathcal{P}$ et de la droite $\mathcal{D}$. Ainsi, $H$ est le projeté orthogonal de $A$ sur la droite $\mathcal{D}$.
	\begin{enumerate}
		\item Montrer que le plan $\mathcal{P}$ admet pour équation cartésienne : $2x-y+2z-3=0$.
		\item En déduire que le point $H$ a pour coordonnées $\left( \dfrac{7}{9};\dfrac{19}{9};\dfrac{16}{9}\right)$.
		\item Calculer la longueur $AH$. On donnera une valeur exacte.
	\end{enumerate}
	\item Dans cette question, on se propose de retrouver les coordonnées du point $H$, projeté orthogonal du point $A$ sur la droite $\mathcal{D}$, par une autre méthode.
	
	\smallskip
	
	On rappelle que le point $B(-1;3;0)$ appartient à la droite $\mathcal{D}$ et que le vecteur $\vect{u}$ est un vecteur directeur de la droite $\mathcal{P}$.
	\begin{enumerate}
		\item Justifier qu’il existe un nombre réel $k$ tel que $\vect{HB}=k\vect{u}$.
		\item Montrer que $k=\dfrac{\vect{AB}\cdot\vect{u}}{||\vect{u}^2||}$.
		\item Calculer la valeur du nombre réel $k$ et retrouver les coordonnées du point $H$. 
	\end{enumerate}
	\item On considère un point $C$ appartenant au plan $\mathcal{P}$ tel que le volume du tétraèdre $ABCH$ soit égal à $\frac89$.
	
	Calculer l’aire du triangle $ACH$.
	
	On rappelle que le volume d’un tétraèdre est donné par : $\mathcal{V}=\dfrac13 \times \mathcal{B} \times h$ où $\mathcal{B}$ désigne l’aire d’une base et $h$ la hauteur relative à cette base.
\end{enumerate}

\pagebreak

\section*{Exercice 3 [Thème : probabilités]\dotfill(7 points)}

\medskip

Le directeur d’une grande entreprise a proposé à l’ensemble de ses salariés un stage de formation à l’utilisation d’un nouveau logiciel.

\smallskip

Ce stage a été suivi par 25\,\% des salariés.

\begin{enumerate}
	\item Dans cette entreprise, 52\,\% des salariés sont des femmes, parmi lesquelles 40\,\% ont suivi le stage.
	
	On interroge au hasard un salarié de l’entreprise et on considère les événements :
	
	\begin{itemize}
		\item $F$ : « le salarié interrogé est une femme » ;
		\item $S$ : « le salarié interrogé a suivi le stage ».
	\end{itemize}
	
	$\overline{F}$ et $\overline{S}$ désignent respectivement les évènements contraires de $F$ et de $S$.
	\begin{enumerate}
		\item Donner la probabilité de l’événement $S$.
		\item Recopier et compléter les pointillés de l’arbre pondéré ci-dessous sur
		les quatre branches indiquées.
		
		\begin{center}
			\begin{forest} for tree = {grow'=0,math content,l=3cm,s sep=0.75cm},
				[\Omega , name=Omega
					[A , fleche , aproba=\ldots , name=A11
						[B , fleche , aproba=\ldots , name=A21]
						[\overline{B} , fleche , bproba=\ldots , name=A22]
					]
					[\overline{A} , fleche , bproba=\ldots , name=A12
						[B , fleche , aproba=\ldots , name=A23]
						[\overline{B} , fleche , bproba=\ldots , name=A24]
					]
				]
			\end{forest}
		\end{center}
		\item Démontrer que la probabilité que la personne interrogée soit une femme ayant suivi le stage est égale à $0,208$.
		\item On sait que la personne interrogée a suivi le stage. Quelle est la probabilité que ce soit une femme ?
		\item Le directeur affirme que, parmi les hommes salariés de l’entreprise, moins de 10\,\% ont suivi le stage.
		
		Justifier l’affirmation du directeur. 
	\end{enumerate}
	\item On note $X$ la variable aléatoire qui à un échantillon de 20 salariés de cette entreprise choisis au hasard associe le nombre de salariés de cet échantillon ayant suivi le stage. On suppose que l’effectif des salariés de l’entreprise est suffisamment important pour assimiler ce choix à un tirage avec remise.
	\begin{enumerate}
		\item Déterminer, en justifiant, la loi de probabilité suivie par la variable aléatoire $X$.
		\item Déterminer, à $10^{-3}$ près, la probabilité que 5 salariés dans un échantillon de 20 aient suivi le stage.
		\item Le programme ci-dessous, écrit en langage \textsf{Python}, utilise la fonction  \texttt{binomiale(i,n,p)} créée pour l’occasion qui renvoie la valeur de la probabilité $P(x=i)$ dans le cas où la variable aléatoire $X$ suit une loi binomiale de paramètres $n$ et $p$.

\begin{CodePythonLstAlt}*[Largeur=8cm]{center}
def proba(k) :
	P = 0
	for i in range(0,k+1) :
		P = P + binomiale(i,20,0.25)
	return P
\end{CodePythonLstAlt}

		Déterminer, à $10^{-3}$ près, la valeur renvoyée par ce programme lorsque l’on saisit \texttt{proba(5)} dans la console \textsf{Python}. Interpréter cette valeur dans le contexte de l’exercice.
		\item Déterminer, à $10^{-3}$ près, la probabilité qu’au moins 6 salariés dans un échantillon de 20 aient suivi le stage.
	\end{enumerate}
	\item Cette question est indépendante des questions \textbf{1} et \textbf{2}.
	
	Pour inciter les salariés à suivre le stage, l’entreprise avait décidé d’augmenter les salaires des salariés ayant suivi le stage de 5\,\%, contre 2\,\% d’augmentation pour les salariés n’ayant pas suivi le stage.
	
	Quel est le pourcentage moyen d’augmentation des salaires de cette entreprise dans ces conditions ?
\end{enumerate}

\pagebreak

\section*{Exercice 4 [Thèmes : géométrie dans le plan et dans l'espace]\dotfill(7 points)}

\medskip

\textit{Cet exercice est un questionnaire à choix multiple.\\
	Pour chaque question, une seule des quatre réponses proposées est exacte. Le candidat indiquera sur sa copie le numéro de la question et la réponse choisie. Aucune justification n’est demandée.\\
	Une réponse fausse, une réponse multiple ou l’absence de réponse à une question ne rapporte ni n’enlève de point.\\
	Les six questions sont indépendantes.}

\begin{enumerate}
	\item La courbe représentative de la fonction $f$ définie sur $\R$ par $f(x)=\dfrac{-2x^2+3x-1}{x^2+1}$ admet pour asymptote la droite d’équation :
	
	\smallskip
	
	\begin{tblr}{width=\linewidth,colspec={X[l,m]X[l,m]}}
		(a)~~$x=-2$&(b)~~$y=-1$ \\
		(c)~~$y=-2$&(d)~~$y=0$
	\end{tblr}
	%
	\item Soit $f$ la fonction définie sur $\R$ par $f(x)=x\,\e^{x^2}$.
	
	La primitive $F$ de $f$ sur $\R$ qui vérifie $F(0)=1$ est définie par :
	
	\begin{tblr}{width=\linewidth,colspec={X[l,m]X[l,m]}}
		(a)~~$F(x)=\dfrac{x^2}{2}\e^{x^2}$&(b)~~$F(x)=\dfrac{1}{2}\e^{x^2}$ \\
		(c)~~$F(x)=\big(1+2x^2\big)\e^{x^2}$&(d)~~$F(x)=\dfrac{1}{2}\e^{x^2}+\dfrac12$
	\end{tblr}
	%
	\item On donne ci-dessous la représentation graphique $\mathcal{C}_{f'}$ de la fonction dérivée $f'$ d’une fonction $f$ définie sur $\R$.
	
	\begin{center}
		\begin{tikzpicture}[x=0.75cm,y=0.75cm,xmin=-1,xmax=10,xgrille=1,xgrilles=0.25,ymin=-5,ymax=1.25,ygrille=1,ygrilles=0.25]
			\GrilleTikz \AxesTikz[ElargirOx=0/0,ElargirOy=0/0] \AxexTikz[Epaisseur=1pt,HautGrad=2pt]{1,2} \AxeyTikz[Epaisseur=1pt,HautGrad=2pt]{1}
			\draw (0,0) node[below left=2pt] {$0$} ;
			\clip (\xmin,\ymin) rectangle (\xmax,\ymax) ;
			\draw[line width=1pt,red,domain=\xmin:\xmax,samples=500] plot (\x,{(2*\x-4)*exp(-0.5*\x)}) ;
			\draw[red] (0.5,-3) node[right,font=\large] {$\mathcal{C}_{f'}$} ;
		\end{tikzpicture}
	\end{center}
	On peut affirmer que la fonction $f$ est :
	
	\begin{tblr}{width=\linewidth,colspec={X[l,m]X[l,m]}}
		(a)~~concave sur $]0;+\infty[$&(b)~~convexe sur $]0;+\infty[$\\
		(c)~~concave sur $[0;2]$&(d)~~convexe sur $[2;+\infty[$
	\end{tblr}
	%
	\item Parmi les primitives de la fonction $f$ définie sur $\R$ par $f(x)=3\,\e{-x^2}+2$ :
	
	\begin{tblr}{width=\linewidth,colspec={X[l,m]X[l,m]}}
		(a)~~toutes sont croissantes sur $\R$&(b)~~toutes sont décroissantes sur $\R$\\
		(c)~~certaines sont croissantes sur $\R$ et d’autres
		décroissantes sur $\R$&(d)~~toutes sont croissantes sur $]0;+\infty[$ et décroissantes sur $]0;+\infty[$
	\end{tblr}
	%
	\item La limite en $+\infty$ de la fonction $f$ définie sur l’intervalle $]0;+\infty[$ par $f(x)=\dfrac{2\,\ln(x)}{3x^2+1}$ est égale à :
	
	\begin{tblr}{width=\linewidth,colspec={X[l,m]X[l,m]X[l,m]X[l,m]}}
		(a)~~$\frac23$&(b)~~$+\infty$&
		(c)~~$-\infty$&(d)~~$0$
	\end{tblr}
	%
	\item L’équation $\e^{2x}+\e^{x}-12=0$ admet dans $\R$ :
	
	\begin{tblr}{width=\linewidth,colspec={X[l,m]X[l,m]X[l,m]X[l,m]}}
		(a)~~trois solutions&(b)~~deux solutions&(c)~~une seule solution&(d)~~aucune solution
	\end{tblr}
\end{enumerate}

\end{document}