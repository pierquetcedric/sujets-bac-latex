Quelques sujets de l'épreuve de Spécialité Maths en Terminale Générale, au format LaTeX et au format pdf.

Les fichiers LaTeX (parfois basés sur ceux de l'APMEP) utilisent des packages courants, le package ProfLycee pour certains environnements (probas, courbes, code Python), et les figures sont réalisées en langage TikZ (les sujets proposés par l'APMEP utilisent le langage pstricks).

Le compilateur en ligne utilisant une version non à jour du package ProfLycee donc une compilation en local pourra générer une erreur (c'est essentiellement pour les environnements CodePythonLst pour lesquels il est désormais nécessaire de préciser les options sous forme de clés).

Les sujets originaux proviennent en majorité des sites ToutMonExam, SujetDeBac ou du site de l'Éducation Nationale.
Je remercie vivement les collègues qui m'ont fait parvenir certains sujets (Nouvelle-Calédonie ou Amérique du Sud notamment).

![illustr](image.jpg)

Les fichiers servent de base au site https://bactex.cpierquet.fr/.

--- Licence CC0 (domaine public) ---
