% !TeX TXS-program:compile = txs:///pdflatex

\documentclass[french,a4paper,11pt]{article}
\usepackage[margin=2cm,includeheadfoot]{geometry}
\usepackage{ProfLycee}
\useproflyclib{ecritures}
\RequirePackage[upright]{fourier}
\usepackage{amsmath,amssymb,amstext}
\usepackage[scaled=0.875]{helvet}
\renewcommand\ttdefault{lmtt}
\usepackage{decimalcomma}
\usepackage{esvect}
\usepackage{tabularx}
\usepackage{multicol}
\usepackage{logoetalab}
\usepackage{scontents}

\newcommand{\session}{2024}
\newcommand{\annee}{2024}
\newcommand{\serie}{Gé.}
\newcommand{\lieu}{Amérique du Nord}
\newcommand{\jour}{21}
\newcommand{\mois}{mai}
\newcommand{\numsujet}{1}
\newcommand{\nomfichier}{[BAC \serie{} \annee{}] \lieu{} (\mois{} \annee)}

\title{\nomfichier}
\usepackage{hyperref}
\usepackage{lastpage}
\usepackage{enumitem}
\usepackage{ibrackets}
\usepackage{customenvs}
\usepackage{fancyhdr}
\usepackage{bm}
\usepackage{tkz-euclide}
\usepackage{babel}

\hypersetup{pdfauthor={Pierquet},pdftitle={\nomfichier},allbordercolors=white,pdfborder=0 0 0,pdfstartview=FitH}
\lhead{\scriptsize\sffamily BAC \serie{} \session}
\chead{\scriptsize\sffamily \lieu{} - Sujet \numsujet}
\rhead{\scriptsize\sffamily \jour{} \mois{} \annee{}}
\lfoot{\scriptsize\sffamily \affloetalab[Legende,TexteLegende={Licence Etalab 2.0}]}
\cfoot{\scriptsize\sffamily 24-MATJ1AN1}
\rfoot{\scriptsize\sffamily - \thepage{}/{}\pageref{LastPage} -}
\setlength{\parindent}{0pt}
%divers
\DeclareMathSymbol{;}\mathbin{operators}{'73}
\newcommand\Rij{\left(O;\vect{\imath},\,\vect{\jmath}\right)}
\newcommand\Rijk{\left(O;\vect{\imath},\,\vect{\jmath},\,\vect{k}\right)}
\newcommand\intervOO[2]{\left]#1;#2\right[}
\newcommand\intervFF[2]{\left[#1;#2\right]}
\newcommand\intervOF[2]{\left]#1;#2\right]}
\newcommand\intervFO[2]{\left[#1;#2\right[}
\newcommand\pta[1]{(#1)}
\newcommand\suiten[1][u]{\left(#1_n\right)}
\newcommand\vect[1]{\vv{#1}}
\usepackage{forest}
\forestset{
	fleche/.style = {edge={thick}},
	aproba/.style = {edge label = {node[above=6pt,pos=.5,fill=white] {$#1$}}},%poids au-dessus
	bproba/.style = {edge label = {node[below=6pt,pos=.5,fill=white] {$#1$}}},%poids en-dessous
}
\tikzset{
	ptcroix/.pic = {
		\draw[rotate = 45] (-#1,0) -- (#1,0);
		\draw[rotate = 45] (0,-#1) -- (0, #1);
	}
}
\newcommand\qcmdeux[4]{%
	\begin{tblr}{width=\linewidth,colspec={*{2}{X[m,l]}}}
		(a)~~#1 & (b)~~#2 \\
		(c)~~#3 & (d)~~#4 \\
	\end{tblr}
}

\newcommand\qcm[4]{%
	\begin{tblr}{width=\linewidth,colspec={*{4}{X[m,l]}}}
		(a)~~#1 & (b)~~#2 & (c)~~#3 & (d)~~#4 \\
	\end{tblr}
}
\usetikzlibrary{hobby}

\begin{document}

\pagestyle{fancy}

{\hfill\Huge \faGraduationCap\hfill~}

\part*{\lieu{}, Bac \serie{}, \jour{} \mois{} \annee, sujet n°\numsujet}

\vspace{0.25cm}

\section*{Exercice 1\dotfill{}(5 points)} %exo1

\medskip

Un jeu vidéo récompense par un objet tiré au sort les joueurs ayant remporté un défi. L'objet tiré peut être « commun » ou « rare ». Deux types d'objets communs ou rares sont disponibles, des épées et des boucliers.

\smallskip

Les concepteurs du jeu vidéo ont prévu que :

\begin{itemize}
	\item la probabilité de tirer un objet rare est de 7\,\% ;
	\item si on tire un objet rare, la probabilité que ce soit une épée est de 80\,\% ;
	\item si on tire un objet commun, la probabilité que ce soit une épée est de 40\,\%. 
\end{itemize}

\textit{Les parties A et B sont indépendantes.}

\medskip

\textbf{\large\underline{Partie A}}

\medskip

Un joueur vient de remporter un défi et tire au sort un objet.

On note:

\begin{itemize}
	\item $R$ l'événement « le joueur tire un objet rare » ;
	\item $E$ l'événement « le joueur tire une épée » ;
	\item $\overline{R}$ et $\overline{E}$ les événements contraires des événements $R$ et $E$.
\end{itemize}

\begin{enumerate}
	\item Dresser un arbre pondéré modélisant la situation, puis calculer $P(R \cap E)$.
	\item Calculer la probabilité de tirer une épée.
	\item Le joueur a tiré une épée. Déterminer la probabilité que ce soit un objet rare. Arrondir le résultat au millième.
\end{enumerate}

\textbf{\large\underline{Partie B}}

\medskip

Un joueur remporte 30 défis.
On note $X$ la variable aléatoire correspondant au nombre d'objets rares que le joueur obtient après avoir remporté 30 défis. Les tirages successifs sont considérés comme indépendants.

\begin{enumerate}
	\item Déterminer, en justifiant, la loi de probabilité suivie par la variable aléatoire $X$.
	
	Préciser ses paramètres, ainsi que son espérance.
	\item Déterminer $P(X < 6)$. \textit{Arrondir le résultat au millième.}
	\item Déterminer la plus grande valeur de $k$ telle que $P(X \geqslant k) \geqslant 0,5$. Interpréter le résultat dans le contexte de l'exercice.
	\item Les développeurs du jeu vidéo veulent proposer aux joueurs d'acheter un « ticket d'or » qui permet de tirer $N$ objets. La probabilité de tirer un objet rare reste de 7\,\%.
	
	Les développeurs aimeraient qu'en achetant un ticket d'or, la probabilité qu'un joueur obtienne au moins un objet rare lors de ces $N$ tirages soit supérieure ou égale à $0,95$.
	
	Déterminer le nombre minimum d'objets à tirer pour atteindre cet objectif. \textit{On veillera à détailler la démarche mise en œuvre.} 
\end{enumerate}

\pagebreak

%\vspace*{5mm}
%
\section*{Exercice 2\dotfill{}(4 points)} %exo2

\medskip

\emph{Cet exercice est un questionnaire à choix multiple.\\ Pour chaque question, une seule des quatre réponses proposées est exacte. Le candidat indiquera sur sa copie le numéro de la question et la réponse choisie. Aucune justification n'est demandée. Une réponse fausse, une réponse multiple ou l'absence de réponse à une question ne rapporte ni n'enlève de point.\\Les cinq questions sont indépendantes.}

\medskip

L'espace est rapporté à un repère orthonormé $\Rijk$.

\begin{enumerate}
	\item On considère les points $A(1;0;3)$ et $B(4;1;0)$.
	
	Une représentation paramétrique de la droite $(AB)$ est :
	
	\begin{MultiCols}[Type=enum](2)
		\item $\begin{dcases}x=3+t\\y=1\\z=-3+3t\end{dcases} \text{ avec }t \in \R$
		\item $\begin{dcases}x=1+4t\\y=t\\z=3\end{dcases} \text{ avec }t \in \R$
		\item $\begin{dcases}x=1+3t\\y=t\\z=3-3t\end{dcases} \text{ avec }t \in \R$
		\item $\begin{dcases}x=4+t\\y=1\\z=3-3t\end{dcases} \text{ avec }t \in \R$
	\end{MultiCols}
\end{enumerate}


On considère la droite $(d)$ de représentation paramétrique $\begin{dcases}x=3+4t\\y=6t\\z=4-2t\end{dcases} \text{ avec }t \in \R$.

\begin{enumerate}[resume]
	\item Parmi les points suivants, lequel appartient à la droite $(d)$ ?
	
	\begin{MultiCols}[Type=enum](4)
		\item $N(3;6;4)$
		\item $M(7;6;6)$
		\item $P(4;6;-2)$
		\item $R(-3;-9;7)$
	\end{MultiCols}
	\item On considère la droite $(d')$ de représentation paramétrique $\begin{dcases}x=-2+3k\\y=-1-2k\\z=1+k\end{dcases} \text{ avec }k \in \R$.
	
	Les droites $(d)$ et $(d')$ sont :
	
	\begin{MultiCols}[Type=enum](4)
		\item sécantes
		\item non coplanaires
		\item parallèles
		\item confondues
	\end{MultiCols}
	\item On considère le plan $(P)$ passant par le point $I(2;1;0)$ et perpendiculaire à la droite $(d)$. Une équation du plan $(P)$ est :
	
	\begin{MultiCols}[Type=enum](2)
		\item $2x + 3y - z -7 = 0$
		\item $-x+ y - 4z + 1 = 0$
		\item $4x +6y-2z + 9 = 0$
		\item $2x+y +1= 0$
	\end{MultiCols}
\end{enumerate}

\newpage

\section*{Exercice 3\dotfill{}(5 points)} %exo3

\medskip

Le but de cet exercice est d'étudier la fonction $f$ définie sur l'intervalle $\intervOO{0}{+\infty}$ par : \[ f(x)=x\,\ln{\big(x^2\big)}-\frac1x. \]

\textbf{\large\underline{Partie A : lectures graphiques}}

\medskip

On a tracé ci-dessous la courbe représentative $\left(C_f\right)$ de la fonction $f$, ainsi que la droite $(T)$, tangente à la courbe $\left(C_f\right)$ au point $A$ de coordonnées $(1;-1)$. Cette tangente passe également par le point $B(0;-4)$.

\begin{Centrage}
	\begin{tikzpicture}[x=2cm,y=0.5cm,xmin=0,xmax=4,xgrille=1,ymin=-7,ymax=7,ygrille=1]
		\FenetreSimpleTikz(ElargirOx=0,ElargirOy=0)<HautGrad=1.5pt>{1,2,3}<HautGrad=1.5pt>{-6,-5,...,6}
		\begin{scope}
			\FenetreTikz
			\draw[line width=1.1pt,red,samples=250,domain=0.05:4] plot (\x,{\x*ln((\x)^2)-1/(\x)}) ;
			\draw[line width=1.1pt,blue,samples=2,domain=0:4] plot (\x,{3*\x-4}) ;
		\end{scope}
		\draw (0,-4) pic[line width=0.8pt] {ptcroix=3pt} node[below right] {$B$} ;
		\draw (1,-1) pic[line width=0.8pt] {ptcroix=3pt} node[below right] {$A$} ;
		\draw (2.25,4.5) node[red] {$\left(C_f\right)$} ;
		\draw (2.75,3.5) node[blue] {$(T)$} ;
	\end{tikzpicture}
\end{Centrage}

\begin{enumerate}
	\item Lire graphiquement $f'(1)$ et donner l'équation réduite de la tangente $(T)$.
	\item Donner les intervalles sur lesquels la fonction $f$ semble convexe ou concave.
	
	Que semble représenter le point A pour la courbe $\left(C_f\right)$ ?
\end{enumerate}

\textbf{\large\underline{Partie A : étude analytique}}

\smallskip

\begin{enumerate}
	\item Déterminer, en justifiant, la limite de $f$ en $+\infty$, puis sa limite en 0.
	\item On admet que la fonction $f$ est deux fois dérivable sur l'intervalle $\intervOO{0}{+\infty}$.
	\begin{enumerate}
		\item Déterminer $f'(x)$ pour $x$ appartenant à l'intervalle $\intervOO{0}{+\infty}$.
		\item Montrer que pour tout $x$ appartenant à l'intervalle $\intervOO{0}{+\infty}$, \[ f''(x) = \frac{2(x+1)(x-1)}{x^3}. \]
	\end{enumerate}
	\item 
	\begin{enumerate}
		\item Étudier la convexité de la fonction $f$ sur l'intervalle $\intervOO{0}{+\infty}$.
		\item Étudier les variations de la fonction $f'$, puis le signe de $f'(x)$ pour $x$ appartenant à l'intervalle $\intervOO{0}{+\infty}$.
		
		En déduire le sens de variation de la fonction $f$ sur l'intervalle $\intervOO{0}{+\infty}$.
	\end{enumerate}
	\item 
	\begin{enumerate}
		\item Montrer que l'équation $f(x)=0$ admet une unique solution $\alpha$ sur l'intervalle $\intervOO{0}{+\infty}$.
		\item Donner la valeur arrondie au centième de $\alpha$ et montrer que $\alpha$ vérifie : \[ \alpha^2 = \text{exp}{\left(\frac{1}{\alpha^2}\right)} \]
	\end{enumerate}
\end{enumerate}

\newpage

\section*{Exercice 4\dotfill{}(6 points)} %exo4

\begin{scontents}[overwrite,write-out=an2024exo4.py]
from math import *
	def seuil() :
	n = 0
	I = 2
	.........
		n = n + 1
		I = ( 1 + exp(-n * pi) ) / (n*n + 1)
	return n
\end{scontents}

\medskip

Pour tout entier naturel $n$, on considère les intégrales suivantes : \[ I_n = \int_0^{\pi} \e^{-nx}\,\sin(x) \dx \] \[J_n = \int_0^{\pi} \e^{-nx}\,\cos(x) \dx \]

\begin{enumerate}
	\item Calculer $I_0$.
	\item 
	\begin{enumerate}
		\item Justifier que, pour tout entier naturel $n$, on $I_n \geqslant 0$.
		\item Montrer que, pour tout entier naturel $n$, on a $I_{n+1}-I_n \leqslant 0$.
		\item Déduire des deux questions précédentes que la suite $\suiten[I]$ converge.
	\end{enumerate}
	\item 
	\begin{enumerate}
		\item Montrer que, pour tout entier naturel $n$, on a : \[ I_n \leqslant \int_0^{\pi} \e^{-nx} \dx. \]
		\item Montrer que, pour tout entier naturel $n \geqslant 1$, on a : \[ \int_0^{\pi} \e^{-nx} \dx = \frac{1-\e^{-n\pi}}{n}. \]
		\item Déduire des deux questions précédentes la limite de la suite $\suiten[I]$.
	\end{enumerate}
	\item 
	\begin{enumerate}
		\item En intégrant par parties l'intégrale $I_n$ de deux façons différentes, établir les deux relations suivantes, pour tout entier naturel $n \geqslant 1$ : \[ I_n = 1+\e^{-n\pi} - n\,J_n \qquad \text{et} \qquad I_n = \frac{1}{n} J_n. \]
		\item En déduire que, pour tout entier naturel $n \geqslant 1$, on a $I_n = \dfrac{1+\e^{-n\pi}}{n^2+1}$.
	\end{enumerate}
	\item On souhaite obtenir le rang \texttt{n} à partir duquel la suite $\suiten[I]$ devient inférieure à $0,1$.
	
	Recopier et compléter la cinquième ligne du script \texttt{Python} ci-dessous avec la commande appropriée.
	
	\CodePythonLstFichierAlt[0.75\linewidth]{center}{an2024exo4.py}
\end{enumerate}

\end{document}