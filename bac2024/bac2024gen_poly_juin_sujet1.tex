% !TeX TXS-program:compile = txs:///pdflatex

\documentclass[french,a4paper,11pt]{article}
\usepackage[margin=2cm,includeheadfoot]{geometry}
\usepackage{microtype}
\usepackage{ProfLycee}
\useproflyclib{ecritures}
\RequirePackage[upright]{fourier}
\usepackage{amsmath,amssymb,amstext}
\usepackage[scaled=0.875]{helvet}
\renewcommand\ttdefault{lmtt}
\usepackage{tkz-grapheur}
\usepackage{decimalcomma}
\usepackage{esvect}
\usepackage{wrapstuff}
\usepackage{tabularx}
\usepackage{multicol}
\usepackage{logoetalab}

\newcommand{\session}{2024}
\newcommand{\annee}{2024}
\newcommand{\serie}{Gé.}
\newcommand{\lieu}{Polynésie}
\newcommand{\jour}{19}
\newcommand{\mois}{juin}
\newcommand{\numsujet}{1}
\newcommand{\nomfichier}{[BAC \serie{} \annee{}] \lieu{} (\mois{} \annee)}

\title{\nomfichier}
\usepackage{hyperref}
\usepackage{lastpage}
\usepackage{enumitem}
\usepackage{ibrackets}
\usepackage{customenvs}
\usepackage{fancyhdr}
\usepackage{FenetreCas}
\usepackage{bm}
\usepackage{tkz-euclide}
\usepackage{babel}

\hypersetup{pdfauthor={Pierquet},pdftitle={\nomfichier},allbordercolors=white,pdfborder=0 0 0,pdfstartview=FitH}
\lhead{\scriptsize\sffamily BAC \serie{} \session}
\chead{\scriptsize\sffamily \lieu{} - Sujet \numsujet}
\rhead{\scriptsize\sffamily \jour{} \mois{} \annee{}}
\lfoot{\scriptsize\sffamily \affloetalab[Legende,TexteLegende={Licence Etalab 2.0}]}
\cfoot{\scriptsize\sffamily 24-MATJ1PO1}
\rfoot{\scriptsize\sffamily - \thepage{}/{}\pageref{LastPage} -}
\setlength{\parindent}{0pt}
%divers
\DeclareMathSymbol{;}\mathbin{operators}{'73}
\newcommand\Rij{\left(O;\vect{\imath},\,\vect{\jmath}\right)}
\newcommand\Rijk{\left(O;\vect{\imath},\,\vect{\jmath},\,\vect{k}\right)}
\newcommand\intervOO[2]{\left]#1;#2\right[}
\newcommand\intervFF[2]{\left[#1;#2\right]}
\newcommand\intervOF[2]{\left]#1;#2\right]}
\newcommand\intervFO[2]{\left[#1;#2\right[}
\newcommand\pta[1]{(#1)}
\newcommand\suiten[1][u]{\left(#1_n\right)}
\newcommand\vect[1]{\vv{#1}}

\begin{document}

\pagestyle{fancy}

{\hfill\Huge \faGraduationCap\hfill~}

\part*{\lieu{}, Bac \serie{}, \jour{} \mois{} \annee, sujet n°\numsujet}

\vspace{0.25cm}

\section*{Exercice 1\dotfill{}(4 points)} %exo1

\medskip

\textit{Pour chacune des affirmations suivantes, indiquer si elle est vraie ou fausse.\\
Chaque réponse doit être justifiée. Une réponse non justifiée ne rapporte aucun point.\\
Dans cet exercice, les questions sont indépendantes les unes des autres.}

\medskip

Les quatre affirmations se placent dans la situation suivante.

\smallskip

Dans l’espace muni d’un repère orthonormé $\Rijk$, on considère les points : \[ A(2;1;-1),~B(-1;2;1) \text{ et } (5;0;-3). \]
%
On note $\mathcal{P}$ le plan d’équation cartésienne : \[ x +5y-2z +3 = 0. \]
%
On note $\mathcal{D}$ la droite de représentation paramétrique : \[ \begin{dcases} x=-3+t \\ y=t+2 \\ z=2t+1 \end{dcases},~t \in \R. \]

\smallskip

\textbf{Affirmation 1 :}

Le vecteur $\Vecteur{n}\begin{pmatrix}1\\0\\2\end{pmatrix}$ est normal au plan $(OAC)$.

\smallskip

\textbf{Affirmation 2 :}

Les droites $\mathcal{D}$ et $(AB)$ sont sécantes au point $C$.

\smallskip

\textbf{Affirmation 3 :}

La droite $\mathcal{D}$ est parallèle au plan $\mathcal{P}$.

\smallskip

\textbf{Affirmation 4 :}

Le plan médiateur du segment $[BC]$, noté $\mathcal{Q}$, a pour équation cartésienne : \[ 3x - y -2z -7 = 0. \]

\textit{On rappelle que le plan médiateur d’un segment est le plan perpendiculaire à ce segment et passant par son milieu.}

\pagebreak

\section*{Exercice 2\dotfill{}(5 points)} %exo2

\medskip

Une entreprise fabrique des objets en plastique en injectant dans un moule de la matière fondue à 210\,°C. On cherche à modéliser le refroidissement du matériau à l’aide d’une fonction $f$ donnant la température du matériau injecté en fonction du temps $t$.

Le temps est exprimé en seconde et la température est exprimée en degré Celsius.

On admet que la fonction $f$ cherchée est solution d’une équation différentielle de la forme suivante où $m$ est une constante réelle que l’on cherche à déterminer : \[ (E)~:~y' +0,02y = m. \]

\textbf{Partie A}

\begin{enumerate}
	\item Justifier l'affichage suivant d'un logiciel de calcul formel :
	
	\begin{Centrage}
		\begin{CalculFormelGeogebra}[PoliceEntete=\bfseries\sffamily]
			\LigneCalculsGeogebra%
				{\small\sffamily RésoudreEquationDifférentielle(y' + 0.02 * y = m)}%
				{$\rightarrow$\:\small\sffamily y = k * exp(-0.02 * t) + 50 * m}
		\end{CalculFormelGeogebra}
	\end{Centrage}
	\item La température de l’atelier est de 30\,°C. On admet que la température $f(t)$ tend vers 30\,°C lorsque $t$ tend vers l’infini.
	
	Démontrer que $m = 0,6$.
	\item Déterminer l’expression de la fonction $f$ cherchée en tenant compte de la condition initiale $f(0) = 210$.
\end{enumerate}

\textbf{Partie B}

\medskip

On admet ici que la température (exprimée en degré Celsius) du matériau injecté en fonction du temps (exprimé en seconde) est donnée par la fonction dont l'expression et une représentation graphique sont données ci-dessous : \[ f(t)=180 \e^{-0,02 t}+30. \]
%
\begin{Centrage}
	\begin{GraphiqueTikz}[x=0.05cm,y=0.02cm,Xmin=0,Xmax=220,Xgrille=20,Xgrilles=20,Ymin=0,Ymax=250,Ygrille=50,Ygrilles=50]
		\TracerAxesGrilles[Elargir=2.5mm]{auto}{auto}
		\draw (\pflxmax,\pflymin) node[above left] {temps (en s)} ;
		\draw (\pflxmin,\pflymax) node[below right] {température (en °C)} ;
		\TracerCourbe[Couleur=red]{180*exp(-0.02*x)+30}
	\end{GraphiqueTikz}
\end{Centrage}

\begin{enumerate}
	\item L'objet peut être démoulé lorsque sa température devient inférieure à 50\,°C.
	\begin{enumerate}
		\item Par lecture graphique, donner une valeur approchée du nombre $T$ de secondes à attendre avant de démouler l'objet.
		\item Déterminer par le calcul la valeur exacte de ce temps $T$.
	\end{enumerate}
	\item À l'aide d'une intégrale, calculer la valeur moyenne de la température sur les 100 premières secondes.
\end{enumerate}

\pagebreak

\section*{Exercice 3\dotfill{}(5 points)} %exo3

\medskip

\textit{Les probabilités demandées seront exprimées sous forme de fractions irréductibles.}

\medskip

\textbf{Partie A}

\medskip

On lance trois fois de suite une pièce de monnaie bien équilibrée. On note $X$ la variable aléatoire qui compte le nombre de fois, sur les trois lancers, où la pièce est retombée du côté « Face ».

\begin{enumerate}
	\item Préciser la nature et les paramètres de la loi de probabilité suivie par $X$.
	\item Recopier et compléter le tableau suivant donnant la loi de probabilité de $X$.
	
	\begin{Centrage}
		\begin{tblr}{width=0.975\linewidth,hlines,vlines,colspec={*{5}{X[m,c]}}}
			$k$ & 0 & 1 & 2 & 3 \\
			$P(X=k)$ &  &  &  &  \\
		\end{tblr}
	\end{Centrage}
\end{enumerate}

\textbf{Partie B}

\medskip

Voici les règles d'un jeu où le but est d'obtenir trois pièces du côté « Face » en un ou deux essais :

\begin{itemize}
	\item on lance trois pièces équilibrées :
	\begin{itemize}
		\item si les trois pièces sont tombées du côté « Face », la partie est gagnée ;
		\item sinon, les pièces tombées du côté « Face » sont conservées et on relance celles tombées du côté {«~Pile~»} ;
	\end{itemize}
	\item la partie est gagnée si on obtient trois pièces du côté « Face », sinon elle est perdue.
\end{itemize}

On considère les évènements suivants :

\begin{itemize}
	\item $G$ : « la partie est gagnée » ;
	
	et pour tout entier $k$ compris entre 0 et 3 , les évènements :
	\item $A_{k}$ : « $k$ pièces sont tombées du côté « Face » au premier lancer ».
\end{itemize}

\begin{enumerate}
	\item Démontrer que $P_{A_{1}}(G)=\frac{1}{4}$.
	\item Recopier et compléter l'arbre pondéré ci-dessous :
	
	\begin{Centrage}
		\begin{center}
			\begin{tikzpicture}
				\tikzstyle{fleche}=[->,>=latex,semithick]
				\tikzstyle{etiquette}=[fill=white,pos=0.6,inner sep=1.75pt,font=\small]
				\def\DistanceInterNiveaux{2.25}
				\def\DistanceInterFeuilles{0.85}
				\def\NiveauA{(0)*\DistanceInterNiveaux}
				\def\NiveauB{(1)*\DistanceInterNiveaux}
				\def\NiveauC{(2)*\DistanceInterNiveaux}
				\def\InterFeuilles{(-1)*\DistanceInterFeuilles}
				\coordinate (R) at ({\NiveauA},{(3)*\InterFeuilles}) ;
				\node[] (Ra) at ({\NiveauB},{(0.5)*\InterFeuilles}) {$A_0$};
				\node[] (Raa) at ({\NiveauC},{(0)*\InterFeuilles}) {$G$};
				\node[] (Rab) at ({\NiveauC},{(1)*\InterFeuilles}) {$\overline{G}$};
				\node[] (Rb) at ({\NiveauB},{(2.5)*\InterFeuilles}) {$A_1$};
				\node[] (Rba) at ({\NiveauC},{(2)*\InterFeuilles}) {$G$};
				\node[] (Rbb) at ({\NiveauC},{(3)*\InterFeuilles}) {$\overline{G}$};
				\node[] (Rc) at ({\NiveauB},{(4.5)*\InterFeuilles}) {$A_2$};
				\node[] (Rca) at ({\NiveauC},{(4)*\InterFeuilles}) {$G$};
				\node[] (Rcb) at ({\NiveauC},{(5)*\InterFeuilles}) {$\overline{G}$};
				\node[] (Rd) at ({\NiveauB},{(6)*\InterFeuilles}) {$A_3$};
				\node[] (Rda) at ({\NiveauC},{(6)*\InterFeuilles}) {$G$};
				% Arcs (MODIFIABLES : Styles)
				\draw[fleche] (R)--(Ra) node[etiquette] {$\tfrac18$};
				\draw[fleche] (Ra)--(Raa);
				\draw[fleche] (Ra)--(Rab);
				\draw[fleche] (R)--(Rb) node[etiquette] {$\tfrac38$};
				\draw[fleche] (Rb)--(Rba);
				\draw[fleche] (Rb)--(Rbb);
				\draw[fleche] (R)--(Rc) node[etiquette] {$\tfrac38$};
				\draw[fleche] (Rc)--(Rca);
				\draw[fleche] (Rc)--(Rcb);
				\draw[fleche] (R)--(Rd) node[etiquette] {$\tfrac18$};
				\draw[fleche] (Rd)--(Rda);
			\end{tikzpicture}
		\end{center}
		%:-+-+-+-+- Fin
	\end{Centrage}
	\item Démontrer que la probabilité $p$ de gagner à ce jeu est $p=\frac{27}{64}$
	\item La partie a été gagnée. Quelle est la probabilité qu'exactement une pièce soit tombée du côté « Face » à la première tentative?
	\item Combien de fois faut-il jouer à ce jeu pour que la probabilité de gagner au moins une partie dépasse $0,95$ ?
\end{enumerate}

\vspace*{5mm}

\section*{Exercice 4\dotfill{}(6 points)} %exo4

\medskip

L'objectif de cet exercice est de conjecturer en \textbf{partie A} puis de démontrer en \textbf{partie B} le comportement d'une suite.

Les deux parties peuvent cependant être traitées de manière indépendante.

On considère la suite $\left(u_{n}\right)$ définie par $u_{0}=3$ et pour tout $n \in \N$ : \[ u_{n+1}=\frac{4}{5-u_{n}}. \]

\textbf{Partie A}

\smallskip

\begin{enumerate}
	\item Recopier et compléter la fonction \textsf{Python} suivante \texttt{suite(n)} qui prend comme paramètre le rang $n$ et renvoie la valeur du terme $u_{n}$.

\begin{CodePythonLstAlt}*[Largeur=7cm]{center}
def suite(n) :
	u = ...
	for i in range(n) :
		...
	return u
\end{CodePythonLstAlt}
	\item L'exécution de \texttt{suite(2)} renvoie \texttt{1.3333333333333333}.
	
	Effectuer un calcul pour vérifier et expliquer cet affichage.
	\item À l'aide des affichages ci-dessous, émettre une conjecture sur le sens de variation et une conjecture sur la convergence de la suite $\left(u_{n}\right)$.
	
\begin{CodePythonLstAlt}*[Largeur=7cm]{center,title={\scriptsize\faCode} Console Python}
>>> suite(2)
1.333333333333333
>>> suite(5)
1.0058479532163742
>>> suite(10)
1.0000057220349845
>>> suite(20)
1.000000000005457
\end{CodePythonLstAlt}
\end{enumerate}

\textbf{Partie B}

\medskip

On considère la fonction $f$ définie et dérivable sur l'intervalle $\IntervalleOO{-\infty}{5}$ par : \[ f(x)=\frac{4}{5-x}. \]
%
Ainsi, la suite $\Suite{u}$ est définie par $u_{0}=3$ et pour tout $n \in \N$, $ u_{n+1}=f\big(u_{n}\big)$.

\begin{enumerate}
	\item Montrer que la fonction $f$ est croissante sur l'intervalle $\IntervalleOO{-\infty}{5}$.
	\item Démontrer par récurrence que pour tout entier naturel $n$ on a : \[ 1 \leqslant u_{n+1} \leqslant u_{n} \leqslant 4. \]
	\item 
	\begin{enumerate}
		\item Soit $x$ un réel de l'intervalle $\IntervalleOO{-\infty}{5}$.
		
		Prouver l'équivalence suivante : \[ f(x)=x \Leftrightarrow x^{2}-5 x+4=0. \]
		\item Résoudre $f (x) = x$ dans l’intervalle $\IntervalleOO{-\infty}{5}$.
	\end{enumerate}
	\item Démontrer que la suite $\Suite{u}$ est convergente.
	
	Déterminer sa limite.
	\item Le comportement de la suite serait-il identique en choisissant comme terme initial $u_0 = 4$ au lieu de $u_0 = 3$ ?
\end{enumerate}

\end{document}