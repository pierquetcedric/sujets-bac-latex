% !TeX TXS-program:compile = txs:///pdflatex

\documentclass[french,a4paper,11pt]{article}
\usepackage[margin=2cm,includeheadfoot]{geometry}
\usepackage{ProfLycee}
\useproflyclib{ecritures}
\RequirePackage[upright]{fourier}
\usepackage{amsmath,amssymb,amstext}
\usepackage[scaled=0.875]{helvet}
\renewcommand\ttdefault{lmtt}
\usepackage{decimalcomma}
\usepackage{customenvs}
\usepackage{esvect}
\usepackage{tabularx}
\usepackage{tikz3d-fr}
\usepackage{multicol}
\usepackage{logoetalab}

\newcommand{\session}{2024}
\newcommand{\annee}{2024}
\newcommand{\serie}{Gé.}
\newcommand{\lieu}{Amérique du Nord}
\newcommand{\jour}{22}
\newcommand{\mois}{mai}
\newcommand{\numsujet}{2}
\newcommand{\nomfichier}{[BAC \serie{} \annee{}] \lieu{} (\mois{} \annee)}

\title{\nomfichier}
\usepackage{hyperref}
\usepackage{lastpage}
\usepackage{enumitem}
\usepackage{ibrackets}
\usepackage{fancyhdr}
\usepackage{bm}
\usepackage{tkz-euclide}
\usepackage{babel}

\hypersetup{pdfauthor={Pierquet},pdftitle={\nomfichier},allbordercolors=white,pdfborder=0 0 0,pdfstartview=FitH}
\lhead{\scriptsize\sffamily BAC \serie{} \session}
\chead{\scriptsize\sffamily \lieu{} - Sujet \numsujet}
\rhead{\scriptsize\sffamily \jour{} \mois{} \annee{}}
\lfoot{\scriptsize\sffamily \affloetalab[Legende,TexteLegende={Licence Etalab 2.0}]}
\cfoot{\scriptsize\sffamily 24-MATJ2AN1}
\rfoot{\scriptsize\sffamily - \thepage{}/{}\pageref{LastPage} -}
\setlength{\parindent}{0pt}
%divers
\DeclareMathSymbol{;}\mathbin{operators}{'73}
\newcommand\Rij{\left(O;\vect{\imath},\,\vect{\jmath}\right)}
\newcommand\Rijk{\left(O;\vect{\imath},\,\vect{\jmath},\,\vect{k}\right)}
\newcommand\intervOO[2]{\left]#1;#2\right[}
\newcommand\intervFF[2]{\left[#1;#2\right]}
\newcommand\intervOF[2]{\left]#1;#2\right]}
\newcommand\intervFO[2]{\left[#1;#2\right[}
\newcommand\pta[1]{(#1)}
\newcommand\suiten[1][u]{\left(#1_n\right)}
\newcommand\vect[1]{\vv{#1}}
\usepackage{forest}
\forestset{
	fleche/.style = {edge={thick}},
	aproba/.style = {edge label = {node[above=6pt,pos=.5,fill=white] {$#1$}}},%poids au-dessus
	bproba/.style = {edge label = {node[below=6pt,pos=.5,fill=white] {$#1$}}},%poids en-dessous
}
\newcommand\qcmdeux[4]{%
	\begin{tblr}{width=\linewidth,colspec={*{2}{X[m,l]}}}
		(a)~~#1 & (b)~~#2 \\
		(c)~~#3 & (d)~~#4 \\
	\end{tblr}
}

\newcommand\qcm[4]{%
	\begin{tblr}{width=\linewidth,colspec={*{4}{X[m,l]}}}
		(a)~~#1 & (b)~~#2 & (c)~~#3 & (d)~~#4 \\
	\end{tblr}
}
\usetikzlibrary{hobby}

\begin{document}

\pagestyle{fancy}

{\hfill\Huge \faGraduationCap\hfill~}

\part*{\lieu{}, Bac \serie{}, \jour{} \mois{} \annee, sujet n°\numsujet}

\vspace{0.25cm}

\section*{Exercice 1\dotfill{}(5 points)} %exo1

\medskip

Les données publiées le 1\up{er} mars 2023 par le ministère de la transition écologique sur les immatriculations de véhicules particuliers en France en 2022 contiennent les informations suivantes :

\begin{itemize}
	\item 22,86\,\% des véhicules étaient des véhicules neufs ;
	\item 8,08\,\% des véhicules neufs étaient des hybrides rechargeables ;
	\item 1,27\,\% des véhicules d'occasion (c'est-à-dire qui ne sont pas neufs) étaient des hybrides rechargeables.
\end{itemize}

\textbf{\textit{Dans tout l'exercice, les probabilités seront arrondies au dix-millième.}}

\medskip

\textbf{\large Partie A}

\medskip

Dans cette partie, on considère un véhicule particulier immatriculé en France en 2022.

On note:

\begin{itemize}
	\item $N$ l'événement « le véhicule est neuf » ;
	\item $R$ l'événement « le véhicule est hybride rechargeable » ;
	\item $\overline{N}$ et $\overline{R}$ les événements contraires des événements contraires de $N$ et $R$.
\end{itemize}

\begin{enumerate}
	\item Représenter la situation par un arbre pondéré.
	\item Calculer la probabilité que ce véhicule soit neuf et hybride rechargeable.
	\item Démontrer que la valeur arrondie au dix-millième de la probabilité que ce véhicule soit hybride rechargeable est \num{0,0283}.
	\item Calculer la probabilité que ce véhicule soit neuf sachant qu'il est hybride rechargeable.
\end{enumerate}

\smallskip

\textbf{\large Partie B}

\medskip

Dans cette partie, on choisit 500 véhicules particuliers hybrides rechargeables immatriculés en France en 2022. Dans la suite, on admettra que la probabilité qu'un tel véhicule soit neuf est égale à 0,65.

On assimile le choix de ces 500 véhicules à un tirage aléatoire avec remise.

\smallskip

On appelle $X$ la variable aléatoire représentant le nombre de véhicules neufs parmi les 500 véhicules choisis.

\begin{enumerate}
	\item On admet que la variable aléatoire $X$ suit une loi binomiale. Préciser la valeur de ses paramètres.
	\item Déterminer la probabilité qu'exactement 325 de ces véhicules soient neufs.
	\item Déterminer la probabilité $p(X \geqslant 325)$ puis interpréter le résultat dans le contexte de l'exercice.
\end{enumerate}

\smallskip

\textbf{\large Partie C}

\medskip

On choisit désormais $n$ véhicules particuliers hybrides rechargeables immatriculés en France en 2022, où $n$ désigne un entier naturel strictement positif.

\smallskip

On rappelle que la probabilité qu'un tel véhicule soit neuf est égale à 0,65.

\smallskip

On assimile le choix de ces n véhicules à un tirage aléatoire avec remise.

\begin{enumerate}
	\item Donner l'expression en fonction de $n$ de la probabilité p, que tous ces véhicules soient d'occasion.
	\item On note $q_n$ la probabilité qu'au moins un de ces véhicules soit neuf. En résolvant une inéquation, déterminer la plus petite valeur de $n$ telle que $q_n \geqslant \num{0,9999}$.
\end{enumerate}

\pagebreak

\section*{Exercice 2\dotfill{}(5 points)} %exo2

\medskip

On considère le pavé droit $ABCDEFGH$ tel que $AB = 3$ et $AD = AE =1$ représenté ci-dessous.

\begin{Centrage}
	\begin{EnvTikzEspace}[UniteX={0:2.75cm},UniteY={65:1.75cm},UniteZ={90:4cm}]
		%placement des points avec labels
		\PlacePointsEspace{A/0,0,0/bg B/3,0,0/bd C/3,1,0/d D/0,1,0/g E/0,0,1/g F/3,0,1/d G/3,1,1/h H/0,1,1/h I/1,0,0/b M/1.5,1,0/h}
		%segments pointillés
		\TraceSegmentsEspace[very thick,dashed]{A/D D/C D/H}
		%segments pleins
		\TraceSegmentsEspace[very thick]{A/B B/C C/G G/H H/E E/A E/F B/F F/G}
		%Marques points
		\MarquePointsEspace[StyleMarque=x,TailleMarque=3pt]{I,M}
	\end{EnvTikzEspace}
\end{Centrage}

On considère le point $I$ du segment $[AB]$ tel que $\vect{AB}=3\vect{AI}$ et on appelle $M$ le milieu du segment $[CD]$.

\smallskip

On se place dans le repère orthonormé $\left(A;\vect{AI},\vect{AD},\vect{AE}\right)$.

\begin{enumerate}
	\item Sans justifier, donner les coordonnées des points $F$, $H$ et $M$.
	\item 
	\begin{enumerate}
		\item Montrer que le vecteur $\vect{n} \begin{pmatrix}2\\6\\3\end{pmatrix}$ est un vecteur normal au plan $(HMF)$.
		\item En déduire qu'une équation cartésienne du plan $(HMF)$ est : \[ 2x + 6y + 3z - 9 = 0. \]
		\item Le plan $(\mathcal{P})$ dont une équation cartésienne est $5x + 15y - 3z + 7 = 0$ est-il parallèle au plan $(HMF)$ ? Justifier la réponse.
	\end{enumerate}
	\item Déterminer une représentation paramétrique de la droite $(DG)$.
	\item On appelle $N$ le point d'intersection de la droite $(DG)$ avec le plan $(HMF)$.
	
	Déterminer les coordonnées du point $N$.
	\item Le point $R$ de coordonnées $\left(3;\frac14;\frac12\right)$ est-il le projeté orthogonal du point $G$ sur le plan $(HMF)$ ? Justifier la réponse.
\end{enumerate}

\pagebreak

\section*{Exercice 3\dotfill{}(5 points)} %exo3

\medskip

On considère la fonction $g$ définie sur l'intervalle $\intervFF{0}{1}$ par $g(x)= 2x - x^2$.

\begin{enumerate}
	\item Montrer que la fonction $g$ est strictement croissante sur l'intervalle $\intervFF{0}{1}$ et préciser les valeurs de $g(0)$ et de $g(1)$.
\end{enumerate}

On considère la suite $\suiten$ définie par $\begin{dcases} u_0=\tfrac12 \\ u_{n+1}=g\big(u_n\big)\end{dcases}$ pour tout entier naturel $n$.

\begin{enumerate}[resume]
	\item Calculer $u_1$ et $u_2$.
	\item Démontrer par récurrence que, pour tout entier naturel $n$, on a : $0 < u_n < u_{n+1} < 1$.
	\item En déduire que la suite $\suiten$ est convergente.
	\item Déterminer la limite $\ell$ de la suite $\suiten$.
\end{enumerate}

On considère la suite $\suiten[v]$ définie pour tout entier naturel $n$ par $v_n = \ln(1-u_n)$.

\begin{enumerate}[resume]
	\item Démontrer que la suite $\suiten[v]$ est une suite géométrique de raison 2 et préciser son premier terme.
	\item En déduire une expression de $v_n$ en fonction de $n$.
	\item En déduire une expression de $u_n$ en fonction de $n$ et retrouver la limite déterminée à la question 5.
	\item Recopier et compléter le script \texttt{Python} ci-dessous afin que celui-ci renvoie le rang \texttt{n} à partir duquel la suite dépasse 0,95.

\begin{CodePythonLstAlt}[Largeur=0.5\linewidth]{center}
def seuil() :
	n = 0
	u = 0.5
	while u < 0.95 :
		n = ......
		u = ......
	return n
\end{CodePythonLstAlt}
\end{enumerate}

\pagebreak

\section*{Exercice 4\dotfill{}(5 points)} %exo4

\medskip

Soit $a$ un réel strictement positif.

On considère la fonction $f$ définie sur l'intervalle $\intervOO{0}{+\infty}$ par $f(x) = a\, \ln(x)$.

On note $\mathcal{C}$ sa courbe représentative dans un repère orthonormé.

Soit $x_0$ un réel strictement supérieur à 1.

\begin{enumerate}
	\item Déterminer l'abscisse du point d'intersection de la courbe $\mathcal{C}$ et de l'axe des abscisses.
	\item Vérifier que la fonction $F$ définie par $F(x) = a(x\,\ln(x) - x)$ est une primitive de la fonction $f$ sur l'intervalle $\intervOO{0}{+\infty}$.
	\item En déduire l'aire du domaine grisé en fonction de $a$ et de $x_0$.
\end{enumerate}

\begin{Centrage}
	\begin{tikzpicture}[x=1.75cm,y=1.75cm,xmin=-1,xmax=5.5,ymin=-1,ymax=2.5]
		\fill[semithick,draw=blue,fill=cyan!50] plot[samples=250,domain=1:1.85] (\x,{ln(\x)}) -- (1.85,0)--cycle ;
		\AxesTikz\AxexTikz{1}
		\draw (0,0) node[below right] {$O$} ;
		\draw (\xmax,0) node[below left] {$x$} ;
		\draw (0,\ymax) node[below left] {$y$} ;
		\draw (1.85,0) node[below] {$x_0$} ;
		\draw[red] (5,1.5) node[below] {$\mathcal{C}$} ;
		\FenetreTikz
		\CourbeTikz[thick,red,samples=250]{ln(\x)}{0.1:5.5}
	\end{tikzpicture}
\end{Centrage}

On note $T$ la tangente à la courbe $\mathcal{C}$ au point $M$ d'abscisse $x_0$..

On appelle $A$ le point d'intersection de la tangente $T$ avec l'axe des ordonnées et $B$ le projeté orthogonal de $M$ sur l'axe des ordonnées.

\begin{Centrage}
	\begin{tikzpicture}[x=1.75cm,y=1.75cm,xmin=-1,xmax=5.5,ymin=-1,ymax=2.5]
		\AxesTikz\AxexTikz{1}
		\draw (0,0) node[below right] {$O$} ;
		\draw (\xmax,0) node[below left] {$x$} ;
		\draw (0,\ymax) node[below left] {$y$} ;
		\draw (1.85,0) node[below] {$x_0$} ;
		\draw[red] (5,1.5) node[below] {$\mathcal{C}$} ;
		\draw[blue] (-0.75,-0.5) node[below] {$T$} ;
		\draw (0,{1*ln(1.85)}) node[above left] {$B$} ;
		\draw (0,{1*ln(1.85)}) rectangle++ (3pt,-3pt) ;
		\draw[dashed] (1.85,0) --++ (0,{1*ln(1.85)});
		\FenetreTikz
		\CourbeTikz[thick,red,samples=250]{ln(\x)}{0.1:5.5}
		\CourbeTikz[thick,blue,samples=2]{1/1.85*(\x-1.85)+1*ln(1.85)}{\xmin:\xmax}
		\CourbeTikz[thick,olive,samples=2]{1*ln(1.85)}{\xmin:\xmax}
		\draw[semithick] (1.85,{1*ln(1.85)}) pic{PLdotcross=3pt/0} node[above] {$M$} ;
		\draw[semithick] (0,{-1+ln(1.85)}) pic{PLdotcross=3pt/0} node[below right] {$A$} ;
	\end{tikzpicture}
\end{Centrage}

\begin{enumerate}[resume]
	\item Démontrer que la longueur $AB$ est égale à une constante (c'est-à-dire à un nombre qui ne dépend pas de $x_0$) que l'on déterminera. \textit{Le candidat prendra soin d'expliciter sa démarche.}
\end{enumerate}

\end{document}