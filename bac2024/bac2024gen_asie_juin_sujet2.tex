% !TeX TXS-program:compile = txs:///pdflatex

\documentclass[french,a4paper,11pt]{article}
\usepackage[margin=2cm,includeheadfoot]{geometry}
\usepackage{ProfLycee}
\useproflyclib{ecritures}
\RequirePackage[upright]{fourier}
\usepackage{amsmath,amssymb,amstext}
\usepackage[scaled=0.875]{helvet}
\renewcommand\ttdefault{lmtt}
\usepackage{decimalcomma}
\usepackage{esvect}
\usepackage{tkz-grapheur}
\usepackage{tikz2d-fr}
\usepackage{tabularx}
\usepackage{multicol}
\usepackage{logoetalab}

\newcommand{\session}{2024}
\newcommand{\annee}{2024}
\newcommand{\serie}{Gé.}
\newcommand{\lieu}{Asie}
\newcommand{\jour}{11}
\newcommand{\mois}{juin}
\newcommand{\numsujet}{2}
\newcommand{\nomfichier}{[BAC \serie{} \annee{}] \lieu{} (\mois{} \annee)}

\title{\nomfichier}
\usepackage{hyperref}
\usepackage{lastpage}
\usepackage{enumitem}
\usepackage{ibrackets}
\usepackage{customenvs}
\usepackage{fancyhdr}
\usepackage{bm}
\usepackage{tkz-euclide}
\usepackage{babel}

\hypersetup{pdfauthor={Pierquet},pdftitle={\nomfichier},allbordercolors=white,pdfborder=0 0 0,pdfstartview=FitH}
\lhead{\scriptsize\sffamily BAC \serie{} \session}
\chead{\scriptsize\sffamily \lieu{} - Sujet \numsujet}
\rhead{\scriptsize\sffamily \jour{} \mois{} \annee{}}
\lfoot{\scriptsize\sffamily \affloetalab[Legende,TexteLegende={Licence Etalab 2.0}]}
\cfoot{\scriptsize\sffamily 24-MATJ2JA1}
\rfoot{\scriptsize\sffamily - \thepage{}/{}\pageref{LastPage} -}
\setlength{\parindent}{0pt}
%divers
\DeclareMathSymbol{;}\mathbin{operators}{'73}
\newcommand\Rij{\left(O;\vect{\imath},\,\vect{\jmath}\right)}
\newcommand\Rijk{\left(O;\vect{\imath},\,\vect{\jmath},\,\vect{k}\right)}
\newcommand\intervOO[2]{\left]#1;#2\right[}
\newcommand\intervFF[2]{\left[#1;#2\right]}
\newcommand\intervOF[2]{\left]#1;#2\right]}
\newcommand\intervFO[2]{\left[#1;#2\right[}
\newcommand\pta[1]{(#1)}
\newcommand\suiten[1][u]{\left(#1_n\right)}
\newcommand\vect[1]{\vv{#1}}
\usepackage{forest}
\forestset{
	fleche/.style = {edge={thick}},
	aproba/.style = {edge label = {node[above=6pt,pos=.5,fill=white] {$#1$}}},%poids au-dessus
	bproba/.style = {edge label = {node[below=6pt,pos=.5,fill=white] {$#1$}}},%poids en-dessous
}
\tikzset{
	ptcroix/.pic = {
		\draw[rotate = 45] (-#1,0) -- (#1,0);
		\draw[rotate = 45] (0,-#1) -- (0, #1);
	}
}
\newcommand\qcmdeux[4]{%
	\begin{tblr}{width=\linewidth,colspec={*{2}{X[m,l]}}}
		(a)~~#1 & (b)~~#2 \\
		(c)~~#3 & (d)~~#4 \\
	\end{tblr}
}

\newcommand\qcm[4]{%
	\begin{tblr}{width=\linewidth,colspec={*{4}{X[m,l]}}}
		(a)~~#1 & (b)~~#2 & (c)~~#3 & (d)~~#4 \\
	\end{tblr}
}
\usetikzlibrary{hobby}

\begin{document}

\pagestyle{fancy}

{\hfill\Huge \faGraduationCap\hfill~}

\part*{\lieu{}, Bac \serie{}, \jour{} \mois{} \annee, sujet n°\numsujet}

\vspace{0.25cm}

\section*{Exercice 1\dotfill{}(5,5 points)} %exo1

\medskip

On considère la fonction $f$ définie sur $\IntervalleOO{0}{+\infty}$ par $f(x) = x^2 - x\,\ln(x)$.

On admet que $f$ est deux fois dérivable sur $\IntervalleOO{0}{+\infty}$.

On note $f'$ la fonction dérivée de la fonction $f$ et $f''$ la fonction dérivée de la fonction $f'$.

\medskip

\textbf{Partie A : Étude de la fonction \bm{$f$}}

\smallskip

\begin{enumerate}
	\item Déterminer les limites de la fonction $f$ en $0$ et en $+\infty$.
	\item Pour tout réel $x$ strictement positif, calculer $f'(x)$.
	\item Montrer que pour tout réel $x$ strictement positif : \[ f'(x)=\frac{2x-1}{x}. \]
	\item Étudier les variations de la fonction $f'$ sur $\IntervalleOO{0}{+\infty}$, puis dresser le tableau des variations de la fonction $f$ sur $\IntervalleOO{0}{+\infty}$.
	
	On veillera à faire apparaître la valeur exacte de l'extremum de la fonction $f'$ sur $\IntervalleOO{0}{+\infty}$.
	
	Les limites de la fonction $f'$ aux bornes de l'intervalle de définition ne sont pas attendues.
	\item Montrer que la fonction $f$ est strictement croissante sur $\IntervalleOO{0}{+\infty}$.
\end{enumerate}

\smallskip

\textbf{Partie B : Étude d'une fonction auxiliaire pour la résolution de l'équation \bm{$f(x) = x$}}

\medskip

On considère dans cette partie la fonction $g$ définie sur $\IntervalleOO{0}{+\infty}$ par $g(x) = x-\ln(x)$.

On admet que la fonction $g$ est dérivable sur $\IntervalleOO{0}{+\infty}$, on note $g'$ sa dérivée.

\begin{enumerate}
	\item Pour tout réel strictement positif, calculer $g'(x)$, puis dresser le tableau des variations de la fonction $g$. Les limites de la fonction $g$ aux bornes de l'intervalle de définition ne sont pas attendues.
	\item On admet que 1 est l'unique solution de l'équation $g(x) = 1$.
	
	Résoudre, sur l'intervalle $\IntervalleOO{0}{+\infty}$, l'équation $f(x) = x$.
\end{enumerate}

\smallskip

\textbf{Partie C : Étude d'une suite récurrente}

\medskip

On considère la suite $\Suite{u}$ définie par $u_0=\frac12$ et pour tout entier naturel $n$, \[ u_{n+1} = f\big(u_n\big)=u_n^2-u_n\,\ln\big(u_n\big). \]

\begin{enumerate}
	\item Montrer par récurrence que pour tout entier naturel $n$ : $\frac12 \leqslant u_n \leqslant u_{n+1} \leqslant 1$.
	\item Justifier que la suite $\Suite{u}$ converge.
\end{enumerate}

On appelle $\ell$ la limite de la suite $\Suite{u}$ et on admet que $\ell$ vérifie l'égalité $f(\ell) =\ell$.

\begin{enumerate}[resume]
	\item Déterminer la valeur de $\ell$.
\end{enumerate}

\pagebreak

\section*{Exercice 2\dotfill{}(5,5 points)} %exo2

\medskip

\emph{%
Léa passe une bonne partie de ses journées à jouer à un jeu vidéo et s'intéresse aux chances de victoire de ses prochaines parties.\\
Elle estime que si elle vient de gagner une partie, elle gagne la suivante dans 70\,\% des cas.\\
Mais si elle vient de subir une défaite, d'après elle, la probabilité qu'elle gagne la suivante est de 0,2.\\
De plus, elle pense avoir autant de chance de gagner la première partie que de la perdre.%
}

\smallskip

On s'appuiera sur les affirmations de Léa pour répondre aux questions de cet exercice.

\smallskip

Pour tout entier naturel $n$ non nul, on définit les événements suivants :

\begin{itemize}
	\item $G_n$ : « Léa gagne la $n$-ième partie de la journée » ;
	\item $D_n$ : « Léa perd la $n$-ième partie de la journée ».
\end{itemize}

Pour tout entier naturel $n$ non nul, on note $g_n$ la probabilité de l'événement $G_n$.

On a donc $g_1 = 0,5$.

\begin{enumerate}
	\item Quelle est la valeur de la probabilité conditionnelle $P_{G_1} \big(D_2\big)$ ?
	\item Recopier et compléter l'arbre des probabilités ci-dessous qui modélise la situation pour les deux premières parties de la journée :
	
	\begin{Centrage}
		\ArbreProbasTikz[PositionProbas=auto]{$G_1$/\numdots/,$G_2$/\numdots/,$D_2$/\numdots/,$D_1$/\numdots/,$G_2$/\numdots/,$D_2$/\numdots/}
	\end{Centrage}
	\item Calculer $g_2$.
	\item Soit $n$ un entier naturel non nul.
	\begin{enumerate}
		\item Recopier et compléter l'arbre des probabilités ci-dessous qui modélise la situation pour les \mbox{$n$-ième} et $(n+1)$-ième parties de la journée.
		
		\begin{Centrage}
			\ArbreProbasTikz[PositionProbas=auto]{$G_n$/\numdots/$g_n$,$G_{n+1}$/\numdots/,$D_{n+1}$/\numdots/,$D_n$/\numdots/,$G_{n+1}$/\numdots/,$D_{n+1}$/\numdots/}
		\end{Centrage}
		\item Justifier que pour tout entier naturel $n$ non nul, $g_{n+1}= 0,5g_n+0,2$.
	\end{enumerate}
	\item Pour tout entier naturel $n$ non nul, on pose $v_n=g_n-0,4$.
	\begin{enumerate}
		\item Montrer que la suite $\Suite{v}$ est géométrique. On précisera son premier terme et sa raison.
		\item Montrer que, pour tout entier naturel $n$ non nul : $g_n = 0,1 \times 0,5^{n-1}+0,4$.
	\end{enumerate}
	\item Étudier les variations de la suite $\Suite{g}$.
	\item Donner, en justifiant, la limite de la suite $\Suite{g}$.
	
	Interpréter le résultat dans le contexte de l'énoncé. 
	\item Déterminer, par le calcul, le plus petit entier $n$ tel que $g_n - 0,4 \leqslant 0,001$.
	\item Recopier et compléter les lignes \texttt{4}, \texttt{5} et \texttt{6} de la fonction suivante, écrite en langage \textsf{Python}, afin qu'elle renvoie le plus petit rang à partir duquel les termes de la suite $\Suite{g}$ sont tous inférieurs ou égaux à
	$0,4 + e$, où $e$ est un nombre réel strictement positif.
\end{enumerate}

\begin{CodePythonLstAlt}[Largeur=7cm,Filigrane]{center}
def seuil(e) :
	g = 0.5
	n = 1
	while ... :
		g = 0.5*g + 0.2
		n = ...
	return n
\end{CodePythonLstAlt}

\pagebreak

\section*{Exercice 3\dotfill{}(4 points)} %exo3

\medskip

Pour chacune des affirmations suivantes, indiquer si elle est vraie ou fausse.

Chaque réponse doit être justifiée. Une réponse non justifiée ne rapporte aucun point.

\medskip

\begin{enumerate}
	\item Soit $\Suite{u}$ une suite définie pour tout entier naturel $n$ et vérifiant la relation suivante : \[\text{pour tout entier naturel }n,\quad \frac12 \leqslant u_n \leqslant \frac{3n^2+4n+7}{6n^2+1}. \]
	\underline{Affirmation 1 :} $\lim\limits_{n \to +\infty} = \frac12$.
	\item Soit une fonction définie et dérivable sur l'intervalle $\IntervalleFF{-4}{4}$.
	
	La représentation graphique $\mathcal{C}_{h'}$ de sa fonction dérivée $h'$est donnée ci-dessous.
	
	\begin{Centrage}
		\begin{GraphiqueTikz}[x=1.8cm,y=0.495cm,Xmin=-4.5,Xmax=4.5,Xgrille=0.5,Xgrilles=0.1,Ymin=-6,Ymax=10.25,Ygrille=2,Ygrilles=0.5]
			\tikzset{pflgrilles/.style={very thin,lightgray!25}}
			\TracerAxesGrilles*[Police=\small]{auto}{auto}
			\TracerCourbe[Couleur=red,Debut=-4,Fin=4]{-0.00000890077723837785*x^9 - 0.00298469080256438*x^8 + 0.000100023513778871*x^7 + 0.0848399565601911*x^6 + 0.0297442435448854*x^5 - 0.652507018715297*x^4 - 0.822087746651839*x^3 + 0.562572122827126*x^2 + 5.30832018398037*x + 4.52414743374050}
			\draw[red] (-3,2) node[font=\Large] {$\mathcal{C}_{h'}$} ;
		\end{GraphiqueTikz}
	\end{Centrage}
	
	\underline{Affirmation 2 :} La fonction $h$ est convexe sur $\IntervalleFF{-1}{3}$.
	\item Le code d'un immeuble est composé de 4 chiffres (qui peuvent être identiques) suivis de deux lettres distinctes parmi A, B et C (exemple : 1232BA).
	
	\smallskip
	
	\underline{Affirmation 3 :} Il existe \num{20634} codes qui contiennent au moins un 0.
	\item On considère la fonction $f$ définie sur $\IntervalleOO{0}{+\infty}$ par $f(x)=x\,\ln(x)$.
	
	\smallskip
	
	\underline{Affirmation 4 :} La fonction $f$ est une solution sur $\IntervalleOO{0}{+\infty}$ de l'équation différentielle \[ x\,y' -y = x. \]
\end{enumerate}

\pagebreak

\section*{Exercice 4\dotfill{}(5 points)} %exo4

\medskip

Dans un repère orthonormé $\Rijk$ de l'espace, on considère le plan $(P)$ d'équation : \[ (P) \text{ : } 2x + 2y - 3z + 1 = 0. \]
%
On considère les trois points $A$, $B$ et $C$ de coordonnées : \[ A(1;0;1)\text{, } B(2;-1;1) \text{ et } C(-4;-6;5). \]
%
Le but de cet exercice est d'étudier le rapport des aires entre un triangle et son projeté orthogonal dans un plan.

\medskip

\textbf{Partie A}

\smallskip

\begin{enumerate}
	\item Pour chacun des points $A$, $B$ et $C$, vérifier s'il appartient au plan $(P)$.
	\item Montrer que le point $C'(0;-2;-1)$ est le projeté orthogonal du point $C$ sur le plan $(P)$.
	\item Déterminer une représentation paramétrique de la droite $(AB)$.
	\item On admet l'existence d'un unique point $H$ vérifiant les deux conditions \[ \begin{dcases} H \in (AB) \\ (AB) \text{ et } (HC) \text{ sont orthogonales} \end{dcases}. \]
	Déterminer les coordonnées du point $H$.
\end{enumerate}

\begin{Centrage}
	\begin{tikzpicture}
		\draw[semithick] (0,0) --++ (6,0) --++ (55:4.5) --++(-6,0) -- cycle ;
		\coordinate (A) at (2.3,0.6) ;
		\coordinate (B) at ($(A)+(28:2.7)$) ;
		\coordinate (C) at ($(B)+(145:2)$) ;
		\coordinate (C') at (3,2) ;
		\draw[semithick] (A) -- (B) -- (C) -- cycle ;
		\draw[semithick,densely dashed] (A) -- (C') -- (B) (C') -- (C) ;
		\MarquerPoints[StyleMarque=+,TailleMarque=1.75pt]{A,B,C,C'}
		\draw (6,0) node[above left] {$(P)$}
		      (A) node[below] {$A$}
		      (B) node[right] {$B$}
		      (C) node[above] {$C$}
		      (C') node[above right] {$C'$} ;
	\end{tikzpicture}
\end{Centrage}

\textbf{Partie B}

\medskip

On admet que les coordonnées du vecteur $\Vecteur{HC}$ sont : $\Vecteur{HC}\begin{pNiceMatrix}[cell-space-limits=1pt]-\tfrac{11}{2}\\-\tfrac{11}{2}\\4\end{pNiceMatrix}$.

\begin{enumerate}
	\item Calculer la valeur exacte de $\Norme{\Vecteur{HC}}$.
	\item Soit $\mathcal{S}$ l'aire du triangle $ABC$. Déterminer la valeur exacte de $\mathcal{S}$.
\end{enumerate}

\smallskip

\textbf{Partie C}

\medskip

On admet que $HC' = \sqrt{\frac{17}{2}}$.

\begin{enumerate}
	\item Soit $\alpha = \widehat{CHC'}$. Déterminer la valeur de $\cos(\alpha)$.
	\item 
	\begin{enumerate}
		\item Montrer que les droites $(C'H)$ et $(AB)$ sont perpendiculaires.
		\item Calculer $\mathcal{S}'$ l'aire du triangle $ABC'$, on donnera la valeur exacte.
		\item Donner une relation entre $\mathcal{S}$, $\mathcal{S}'$ et $\cos(\alpha)$.
	\end{enumerate}
\end{enumerate}

\end{document}