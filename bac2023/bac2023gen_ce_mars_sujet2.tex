% !TeX TXS-program:compile = txs:///pdflatex

\documentclass[french,a4paper,11pt]{article}
\usepackage[margin=2cm,includeheadfoot]{geometry}
\usepackage{ProfLycee}
\RequirePackage[upright]{fourier}
\usepackage{amsmath,amssymb,amstext}
\usepackage[scaled=0.875]{helvet}
\renewcommand\ttdefault{lmtt}
\usepackage{esvect}
\usepackage{logoetalab}

\newcommand{\session}{2023}
\newcommand{\annee}{2023}
\newcommand{\serie}{Gé.}
\newcommand{\lieu}{Centres Étrangers}
\newcommand{\jour}{14}
\newcommand{\mois}{Mars}
\newcommand{\numsujet}{2}
\newcommand{\nomfichier}{[BAC \serie{} \annee{}] \lieu{} (\mois{} \annee)}

\title{\nomfichier}
\usepackage{hyperref}
\usepackage{lastpage}
\usepackage{enumitem}
\usepackage{ibrackets}
\usepackage{fancyhdr}
\usepackage{babel}

\hypersetup{pdfauthor={Pierquet},pdftitle={\nomfichier},allbordercolors=white,pdfborder=0 0 0,pdfstartview=FitH}
\lhead{\scriptsize\sffamily BAC \serie{} \session}
\chead{\scriptsize\sffamily \lieu{} - Sujet \numsujet}
\rhead{\scriptsize\sffamily \jour{} \mois{} \annee{}}
\lfoot{\scriptsize\sffamily \affloetalab[Legende,TexteLegende={Licence Etalab 2.0}]}
\cfoot{\scriptsize\sffamily 23-MATJ2G11}
\rfoot{\scriptsize\sffamily - \thepage{}/{}\pageref{LastPage} -}
\setlength{\parindent}{0pt}
%divers
\DeclareMathSymbol{;}\mathbin{operators}{'73}
\newcommand\vect[1]{\vv{#1}}

\begin{document}

\pagestyle{fancy}

%\vspace*{0.5cm}

{\hfill\Huge \faGraduationCap\hfill~}

\part*{\lieu{}, Bac \serie{}, \jour{} \mois{} \annee, sujet n°\numsujet}

\vspace{0.25cm}

\section*{Exercice 1 [QCM]\dotfill(5 points)}

\medskip

\emph{Cet exercice est un questionnaire à choix multiples. Pour chaque question, une seule des quatre propositions est exacte. Indiquer sur la copie le numéro de la question et la lettre de la proposition choisie. Aucune justification n'est demandée.\\
Pour chaque question, une réponse exacte rapporte un point. Une réponse fausse, une réponse multiple ou l'absence de réponse ne rapporte ni n'enlève de point.}

\bigskip

\textbf{Question 1 :}

\smallskip

Soit $f$ la fonction définie sur $\mathbb{R}$ par $f(x)=x\,\text{e}^x$. Une primitive $F$ sur $\mathbb{R}$ de la fonction $f$ est définie par :

\medskip

\begin{tblr}{width=\linewidth,colspec={X[l,m]X[l,m]}}%X[l,m]X[l,m]}}
	(a)~~$F(x)=\frac{x^2}{2}\text{e}^x$ & (b)~~$F(x)=(x-1)\text{e}^x$ \\
	(c)~~$F(x)=(x+1)\text{e}^x$ & (d)~~$F(x)=\frac12x\,\text{e}^{x^2}$
\end{tblr}

\bigskip

\textbf{Question 2 :}

\medskip

La courbe $\mathcal{C}$ ci-dessous représente une fonction $f$ définie et deux fois dérivable sur $]0;+\infty[$.

On sait que :

\begin{itemize}
	\item le maximum de la fonction $f$ est atteint au point d'abscisse 3 ;
	\item le point $P$ d'abscisse 5 est l'unique point d'inflexion de la courbe $\mathcal{C}$.
\end{itemize}

\begin{center}
	\begin{tikzpicture}[x=0.5cm,y=0.5cm,xmin=-1,xmax=14,xgrille=1,xgrilles=0.2,ymin=-3,ymax=6,ygrille=1,ygrilles=0.2]
		\GrilleTikz[][very thin,gray][very thin,lightgray]
		\AxesTikz[Epaisseur=0.75pt,ElargirOx=0/0,ElargirOy=0/0]
		\OrigineTikz[Police=\tiny,Decal=0pt]
		\AxexTikz[Epaisseur=0.75pt,AffOrigine=false,Police=\tiny,HautGrad=3pt]{-1,0,...,13}
		\AxeyTikz[Epaisseur=0.75pt,AffOrigine=false,Police=\tiny,HautGrad=3pt]{-3,-2,...,5}
		\FenetreTikz
		\CourbeTikz[thick,red,samples=500]{10*(\x-1)*exp(-0.5*\x)}{0:14}
		\draw[<->,>=latex,CouleurVertForet,thick] ({2.25},{(10*3-10)*exp(-0.5*3)}) -- ({3.75},{(10*3-10)*exp(-0.5*3)}) ;
		\draw[red] (10,1.25) node[below right=0pt,font=\scriptsize] {$y=f(x)$} ;
		\CourbeTikz[thick,densely dashed,blue,samples=2]{-10*exp(-2.5)*(\x-5)+40*exp(-2.5)}{0:14}
		\filldraw (5,{(10*5-10)*exp(-0.5*5)}) circle[radius=1.5pt] node[above right,font=\scriptsize] {$P$} ;
	\end{tikzpicture}
\end{center}

On a :

\begin{tblr}{width=\linewidth,colspec={X[l,m]}}
	(a)~~pour tout réel $x \in ]0;5[$, $f(x)$ et $f'(x)$ sont de même signe \\
	(b)~~pour tout réel $x \in ]5;+\infty[$, $f(x)$ et $f'(x)$ sont de même signe \\
	(c)~~pour tout réel $x \in ]0;5[$, $f'(x)$ et $f''(x)$ sont de même signe \\
	(d)~~pour tout réel $x \in ]5;+\infty[$, $f(x)$ et $f''(x)$ sont de même signe
\end{tblr}

\pagebreak

\textbf{Question 3 :}

\medskip

On considère la fonction $g$ définie sur $[0;+\infty[$ par $g(t)=\frac{a}{b+\text{e}^{-t}}$ où $a$ et $b$ sont deux nombres réels.

On sait que $g(0)=2$ et $\displaystyle\lim_{t \to +\infty} g(t)=3$. Les valeurs de $a$ et $b$ sont :

\medskip

\begin{tblr}{width=\linewidth,colspec={X[l,m]X[l,m]}}
	(a)~~$a=2$ et $b=3$ & (b)~~$a=4$ et $b=\frac43$ \\
	(c)~~$a=4$ et $b=1$ & (d)~~$a=6$ et $b=2$
\end{tblr}

\bigskip

\textbf{Question 4 :}

\medskip

Alice dispose de deux urnes A et B contenant chacune quatre boules indiscernables au toucher. L'urne A contient deux boules vertes et deux boules rouges. L'urne B contient trois boules vertes et une boule rouge.

\smallskip

Alice choisit au hasard une urne puis une boule dans cette urne. Elle obtient une boule verte. La probabilité qu'elle ait choisi l'urne B est :

\medskip

\begin{tblr}{width=\linewidth,colspec={X[l,m]X[l,m]}}
	(a)~~$\frac38$ & (b)~~$\frac12$ \\
	(c)~~$\frac35$ & (d)~~$\frac58$ \\
\end{tblr}

\bigskip

\textbf{Question 5 :}

\medskip

On pose $S=1+\frac12+\frac13+\frac14+\ldots+\frac{1}{100}$.

\smallskip

Parmi les scripts \textsf{Python} ci-dessous, celui est permet de calculer la somme $S$ est :

\begin{tblr}{width=\linewidth,colspec={X[l,m]X[l,m]}}
	(a)~~ & (b)~~
\end{tblr}

\begin{minipage}{0.5\linewidth}
\begin{CodePythonLstAlt}[Largeur=8cm]{}
def somme_a() :
	S = 0
	for k in range(100) :
		S = 1/(k+1)
	return S
\end{CodePythonLstAlt}
\end{minipage}
\begin{minipage}{0.5\linewidth}
\begin{CodePythonLstAlt}[Largeur=8cm]{}
def somme_b() :
	S = 0
	for k in range(100) :
		S = S + 1/(k+1)
	return S
\end{CodePythonLstAlt}
\end{minipage}

\begin{tblr}{width=\linewidth,colspec={X[l,m]X[l,m]}}
	(c)~~ & (d)~~
\end{tblr}

\begin{minipage}{0.5\linewidth}
\begin{CodePythonLstAlt}[Largeur=8cm]{}
def somme_c() :
	k = 0
	while S < 100 :
		S = S + 1/(k+1)
	return S
\end{CodePythonLstAlt}
\end{minipage}
\begin{minipage}{0.5\linewidth}
\begin{CodePythonLstAlt}[Largeur=8cm]{}
def somme_d() :
	k = 0
	while k < 100 :
		S = S + 1/(k+1)
	return S
\end{CodePythonLstAlt}
\end{minipage}

\pagebreak

\section*{Exercice 2 \dotfill(6 points)}

\medskip

On considère la fonction $f$ définie sur $]-1,5 ; +\infty[$ par $f(x)=\ln(2x+3)-1$.

\smallskip

Le but de cet exercice est d'étudier la convergence de la suite $\left(u_n\right)$ définie par :%
\[ u_0=0 \text{ et } u_{n+1} = f{\big(u_n\big)} \text{ pour tout entier naturel } n. \]
%
\textbf{Partie A : Étude d'une fonction auxiliaire}

\medskip

On considère la fonction $g$ définie sur $]-1,5 ; +\infty[$ par $g(x) = f (x) - x$.

\begin{enumerate}
	\item Déterminer la limite de la fonction $g$ en $-1,5$.
\end{enumerate}

On admet que la limite de la fonction $g$ en $+\infty$ est $-\infty$.

\begin{enumerate}[resume]
	\item Étudier les variations de la fonction $g$ sur $]-1,5 ; +\infty[$.
	\item 
	\begin{enumerate}
		\item Démontrer que, dans l'intervalle $]-0,5 ; +\infty[$, l'équation $g(x)=0$ admet une unique solution $\alpha$.
		\item Déterminer un encadrement de $\alpha$ d'amplitude $10^{-2}$.
	\end{enumerate}
\end{enumerate}

\textbf{Partie B : Étude de la suite \boldmath$\left(u_n\right)$\unboldmath}

\medskip

On admet que la fonction $f$ est strictement croissante sur $]-1,5 ; +\infty[$.

\begin{enumerate}
	\item Soit $x$ un nombre réel. Montrer que si $x \in [-1;\alpha]$ alors $f(x) \in [-1; \alpha]$.
	\item 
	\begin{enumerate}
		\item Démontrer par récurrence que pour tout entier naturel $n$ : \[ -1 \leqslant u_n \leqslant u_{n+1} \leqslant \alpha. \]
		\item En déduire que la suite $\left(u_n\right)$ converge.
	\end{enumerate}
\end{enumerate}

\pagebreak

\section*{Exercice 3 \dotfill(6 points)}

\medskip

La figure ci-dessous correspond à la maquette d'un projet architectural.
Il s'agit d'une maison de forme cubique $(ABCDEFGH)$ accolée à un garage de forme cubique $(BIJKLMNO)$ où $L$ est le milieu du segment $[BF]$ et $K$ est le milieu du segment $[BC]$.

Le garage est surmonté d'un toit de forme pyramidale $(LMNOP)$ de base carrée $LMNO$ et de sommet $P$ positionné sur la façade de la maison.

\begin{center}
	\begin{tikzpicture}[x={(-45:12.5mm)},y={(30:14mm)},z={(90:10mm)},line join=bevel]
		\coordinate (A) at (0,0,0) ;
		\coordinate (B) at (2,0,0) ;
		\coordinate (C) at (2,2,0) ;
		\coordinate (D) at (0,2,0) ;
		\coordinate (E) at (0,0,2) ;
		\coordinate (F) at (2,0,2) ;
		\coordinate (G) at (2,2,2) ;
		\coordinate (H) at (0,2,2) ;
		\coordinate (L) at (2,0,1) ;
		\coordinate (K) at (2,1,0) ;
		\coordinate (I) at (3,0,0) ;
		\coordinate (M) at (3,0,1) ;
		\coordinate (J) at (3,1,0) ;
		\coordinate (N) at (3,1,1) ;
		\coordinate (O) at (2,1,1) ;
		\coordinate (P) at (2,{2/3},{4/3}) ;
		\coordinate (Q) at (2,1.55,0) ;
		\fill[draw=none,semithick,fill=lightgray!25] (A)--(B)--(F)--(E)--cycle ;
		\fill[draw=none,semithick,fill=lightgray!25] (E)--(F)--(G)--(H)--cycle ;
		\fill[draw=none,semithick,fill=lightgray!25] (B)--(C)--(G)--(F)--cycle ;
		\fill[draw=none,semithick,fill=lightgray!25] (B)--(L)--(M)--(I)--cycle ;
		\fill[draw=none,semithick,fill=lightgray!25] (I)--(J)--(N)--(M)--cycle ;
		\fill[draw=none,semithick,fill=lightgray!75] (L)--(M)--(N)--(O)--(P)--cycle ;
		\draw[->,>=latex] (A)--(1,0,0) node[pos=0.4,below=2pt,font=\scriptsize] {$\vect{\imath}$} ;
		\draw[->,>=latex,densely dashed] (A)--(0,1,0) node[midway,above,font=\scriptsize] {$\vect{\jmath}$} ;
		\draw[->,>=latex] (A)--(0,0,1) node[midway,left,font=\scriptsize] {$\vect{k}$} ;
		\draw[thick] (A)--(B)--(F)--(E)--cycle ;
		\draw[thick] (E)--(F)--(G)--(H)--cycle ;
		\draw[thick] (B)--(F)--(G)--(C) ;
		\draw[thick,dashed] (B)--(C) (A)--(D)--(H) (D)--(C) (B)--(K)--(J);
		\draw[thick,gray] (L)--(M)--(N)--(O)--(P)--cycle (P)--(M) (P)--(N) ;
		\draw[thick,gray,dashed] (L)--(O)--(K) ;
		\draw[thick] (B)--(I)--(M)--(L)--cycle (Q)--(C)--(G) ;
		\draw[thick] (I)--(J)--(N)--(M)--cycle ;
		\draw[->,>=latex,semithick] (A)--(4,0,0) ;
		\draw[->,>=latex,semithick] (A)--(0,0,3) ;
		\draw[semithick,gray,densely dashed,] (D)--(0,3.05,0) ;
		\draw[->,>=latex,semithick] (0,3.05,0)--(0,4.5,0) ;
		\foreach \point/\pos in {A/left,B/below,C/right,D/above left,E/left,F/above,G/right,H/above,I/below,J/right,K/below,L/left,M/below right,N/above right,O/above,P/above}
			{\draw (\point) node[font=\scriptsize,\pos] {$\point$} ;}
	\end{tikzpicture}
\end{center}

\smallskip

On munit l'espace du repère orthonormé $\left(A;\vect{\imath},\vect{\jmath},\vect{k}\right)$ avec $\vect{\imath} = \frac12\vect{AB}$, $\vect{\jmath} = \frac12\vect{AD}$ et $\vect{k} = \frac12\vect{AE}$.

\begin{enumerate}
	\item 
	\begin{enumerate}
		\item Par lecture graphique, donner les coordonnées des points $H$, $M$ et $N$.
		\item Déterminer une représentation paramétrique de la droite $(HM)$.
	\end{enumerate}
	\item L'architecte place le point $P$ à l'intersection de la droite $(HM)$ et du plan $(BCF)$.
	
	Montrer que les coordonnées de $P$ sont $\left(2;\frac23;\frac43\right)$.
	\item 
	\begin{enumerate}
		\item Calculer le produit scalaire $\vect{PM}\cdot\vect{PN}$.
		\item Calculer la distance $PM$.
	\end{enumerate}
	On admet que la distance $PN$ est égale à $\frac{\sqrt{11}}{3}$.
	\begin{enumerate}[resume]
		\item Pour satisfaire à des contraintes techniques, le toit ne peut être construit que si l'angle $\widehat{MPN}$ ne dépasse pas \ang{55}. Le toit pourra-t-il être construit ?
	\end{enumerate}
	\item Justifier que les droites $(HM)$ et $(EN)$ sont sécantes. Quel est leur point d'intersection ?
\end{enumerate}

\pagebreak

\section*{Exercice 4 \dotfill(3 points)}

\medskip

Une société de production s'interroge sur l'opportunité de programmer un jeu télévisé. Ce jeu réunit quatre candidats et se déroule en deux phases.

\smallskip

La première phase est une phase de qualification. Cette phase ne dépend que du hasard'

Pour chaque candidat, la probabilité de se qualifier est $0,6$.

\smallskip

La deuxième phase est une compétition entre les candidats qualifiés. Elle n'a lieu que si deux candidats au moins sont qualifiés. Sa durée dépend du nombre de candidats qualifiés comme l'indique le tableau ci-dessous (lorsqu'il n'y a pas de deuxième phase, on considère que sa durée est nulle).

\begin{center}
	\begin{tblr}{hlines,vlines,width=12cm,colspec={Q[5.5cm,l,m]*{5}{X[m,c]}}}
		Nombre de candidats qualifiés pour la deuxième phase	& 0 & 1 & 2 & 3 & 4 \\
		Durée de la deuxième phase en minutes					& 0 & 0 & 5 & 9 & 11 \\
	\end{tblr}
\end{center}

Pour que la société décide de retenir ce jeu, il faut que les deux conditions suivantes soient vérifiées :

\smallskip

\underline{Condition n°1} : La deuxième phase doit avoir lieu dans au moins 80\,\% des cas

\underline{Condition n°2} : La durée moyenne de la deuxième phase ne doit pas excéder 6 minutes.

\medskip

\textbf{Le jeu peut-il être retenu ?}

\end{document}