% !TeX TXS-program:compile = txs:///pdflatex

\documentclass[french,a4paper,11pt]{article}
\usepackage[margin=2cm,includeheadfoot]{geometry}
\usepackage{ProfLycee}
\RequirePackage[upright]{fourier}
\usepackage{amsmath,amssymb,amstext}
\usepackage[scaled=0.875]{helvet}
\renewcommand\ttdefault{lmtt}
\usepackage{decimalcomma}
\usepackage{esvect}
\usepackage{logoetalab}

\usepackage{enumitem}
\setlist[enumerate,1]{label=\bfseries\arabic*.}
\setlist[enumerate,2]{label=\bfseries\alph*.}

\newcommand{\session}{2023}
\newcommand{\annee}{2023}
\newcommand{\serie}{Gé.}
\newcommand{\lieu}{Liban}
\newcommand{\jour}{22}
\newcommand{\mois}{Mars}
\newcommand{\numsujet}{2}
\newcommand{\nomfichier}{[BAC \serie{} \annee{}] \lieu{} (\mois{} \annee)}

\title{\nomfichier}
\usepackage{hyperref}
\usepackage{lastpage}
\usepackage{enumitem}
\usepackage{ibrackets}
\usepackage{fancyhdr}
\usepackage{babel}

\hypersetup{pdfauthor={Pierquet},pdftitle={\nomfichier},allbordercolors=white,pdfborder=0 0 0,pdfstartview=FitH}
\lhead{\scriptsize\sffamily BAC \serie{} \session}
\chead{\scriptsize\sffamily \lieu{} - Sujet \numsujet}
\rhead{\scriptsize\sffamily \jour{} \mois{} \annee{}}
\lfoot{\scriptsize\sffamily \affloetalab[Legende,TexteLegende={Licence Etalab 2.0}]}
\cfoot{\scriptsize\sffamily 23-MATJ2LI1}
\rfoot{\scriptsize\sffamily - \thepage{}/{}\pageref{LastPage} -}
\setlength{\parindent}{0pt}
%divers
\DeclareMathSymbol{;}\mathbin{operators}{'73}
\newcommand\vect[1]{\vv{#1}}

\begin{document}

\pagestyle{fancy}

{\hfill\Huge \faGraduationCap\hfill~}

\part*{\lieu{}, Bac \serie{}, \jour{} \mois{} \annee, sujet n°\numsujet}

\vspace{0.25cm}

\section*{Exercice 1 \dotfill(5 points)}

\medskip

On considère la fonction $f$ définie sur $\mathbb{R}$ par : \[ f(x)=\frac{1}{1+\text{e}^{-3x}}. \]
%
On note $\mathcal{C}_f$ sa courbe représentative dans un repère orthogonal du plan.

On nomme $A$ le point de coordonnées $\left(0;\frac12\right)$ et $B$ le point de coordonnées $\left(1;\frac54\right)$.

On a tracé ci-dessous la courbe $\mathcal{C}_f$ et $\mathcal{T}$ la tangente à la courbe $\mathcal{C}_f$, au point d'abscisse $0$.

\begin{center}
	\begin{tikzpicture}[x=1.5cm,y=3cm,xmin=-3,xmax=4,xgrille=0.5,xgrilles=0.5,ymin=-0.75,ymax=1.5,ygrille=0.25,ygrilles=0.25]
		\GrilleTikz \AxesTikz[ElargirOx=0/0,ElargirOy=0/0]
		\draw (0,0) node[below left=1pt] {$0$} ;
		\draw[thick] (1,1.75pt)--++(0,-3.5pt) node[below] {$1$} ;
		\draw[thick] (1.75pt,1)--++(-3.5pt,0) node[left] {$1$} ;
		\clip (\xmin,\ymin) rectangle (\xmax,\ymax) ;
		\draw[thick,red,samples=250,domain=\xmin:\xmax] plot (\x,{1/(1+exp(-3*\x))}) ;
		\draw[thick,blue,samples=2] plot (\x,{0.75*\x+0.5}) ;
		\filldraw (0,0.5) circle[radius=1.5pt] node[below right] {$A$} ;
		\filldraw (1,1.25) circle[radius=1.5pt] node[below right] {$B$} ;
		\draw (3.75,1.125) node[red] {$\mathcal{C}_f$} ;
		\draw (-1.25,-0.625) node[blue] {$\mathcal{T}$} ;
	\end{tikzpicture}
\end{center}

\medskip

\textbf{Partie A : lectures graphiques}

\medskip

Dans cette partie, les résultats seront obtenus par lecture graphique. Aucune justification n'est demandée.

\begin{enumerate}
	\item Déterminer l'équation réduite de la tangente $\mathcal{T}$.
	\item Donner les intervalles sur lesquels la fonction $f$ semble convexe ou concave.
\end{enumerate}

\smallskip

\textbf{Partie B : Étude de la fonction}

\smallskip

\begin{enumerate}
	\item On admet que la fonction $f$ est dérivable sur $\mathbb{R}$.
	
	Déterminer l'expression de sa fonction dérivée $f'$.
	\item Justifier que la fonction $f$ est strictement croissante sur $\mathbb{R}$.
	\item 
	\begin{enumerate}
		\item Déterminer la limite en $+\infty$ de la fonction $f$.
		\item Déterminer la limite en $-\infty$ de la fonction $f$.
	\end{enumerate}
	\item D~terminer la valeur exacte de la solution $\alpha$ de l'équation $f(x) = 0,99$.
\end{enumerate}

\pagebreak

\textbf{Partie C : Tangente et convexité}

\smallskip

\begin{enumerate}
	\item Déterminer par le calcul une équation de la tangente $\mathcal{T}$ à la courbe $\mathcal{C}_f$ au point d'abscisse $0$.
\end{enumerate}

On admet que la fonction $f$ est deux fois dérivable sur $\mathbb{R}$. On note $f''$ la fonction dérivée seconde de la fonction $f$.

On admet que $f''$ est définie sur $\mathbb{R}$ par : \[ f''(x)=\frac{9\text{e}^{-3x}\big(\text{e}^{-3x}-1\big)}{\big(1+\text{e}^{-3x}\big)^3}. \]
%
\begin{enumerate}[resume]
	\item Étudier le signe de la fonction $f''$ sur $\mathbb{R}$.
	\item 
	\begin{enumerate}
		\item Indiquer, en justifiant, sur quel(s) intervalle(s) la fonction $f$ est convexe.
		\item Que représente le point $A$ pour la courbe $\mathcal{C}_f$ ?
	\end{enumerate}
	\item En déduire la position relative de la tangente $\mathcal{T}$ et de la courbe $\mathcal{C}_f$. Justifier la réponse. 
\end{enumerate}

\vspace*{5mm}

\section*{Exercice 2 \dotfill(5 points)}

\medskip

\textbf{Partie A}

\medskip

On considère la fonction $f$ définie par : \[ f(x)=x-\ln(1+x). \]
%
\begin{enumerate}
	\item Justifier que la fonction $f$ est définie sur l'intervalle $]-1;+\infty[$.
	\item On admet que la fonction $f$ est dérivable sur $]-1;+\infty[$.
	
	Déterminer l'expression de sa fonction dérivée $f'$.
	\item 
	\begin{enumerate}
		\item En déduire le sens de variation de la fonction $f$ sur l'intervalle $]-1;+\infty[$.
		\item En déduire le signe de la fonction $f$ sur l'intervalle $]-1;+\infty[$.
	\end{enumerate}
	\item 
	\begin{enumerate}
		\item Montrer que, pour tout $x$ appartenant à l'intervalle $]-1;+\infty[$[, on a : \[ f(x) = \ln {\left(\frac{\text{e}^x}{1+x}\right)}. \]
		\item En déduire la limite en $+\infty$ de la fonction $f$.
	\end{enumerate}
\end{enumerate}

\medskip

\textbf{Partie B}

\medskip

On considère la suite $\big(u_n\big)$ définie par $u_0=10$ et, pour tout entier naturel $n$, \[ u_{n+1}=u_n-\ln\big(1+u_n\big). \]
%
On admet que la suite $\big(u_n\big)$ est bien définie.

\begin{enumerate}
	\item Donner la valeur arrondie au millième de $u_1$.
	\item En utilisant la question \textbf{3.a.} de la \textbf{partie A}, démontrer par récurrence que, pour tout entier naturel $n$, on a $u_n \geqslant 0$.
	\item Démontrer que la suite $\big(u_n\big)$ est décroissante.
	\item Déduire des questions précédentes que la suite $\big(u_n\big)$ converge.
	\item Déterminer la limite de la suite $\big(u_n\big)$.
\end{enumerate}

\pagebreak

\section*{Exercice 3 \dotfill(5 points)}

\medskip

L'espace est muni d'un repère orthonormé $\left(O;\,\vect{\imath},\,\vect{\jmath},\,\vect{k}\right)$

On considère les points $A(3;0;1)$, $B(2;1;2)$ et $C(-2;-5;1)$.

\begin{enumerate}
	\item Démontrer que les points $A$, $B$ et $C$ ne sont pas alignés.
	\item Démontrer que le triangle $ABC$ est rectangle en $A$.
	\item Vérifier que le plan $(ABC)$ a pour équation cartésienne : \[ -x + y - 2 z + 5 = 0. \]
	\item On considère le point $S(1;-2;4)$.
	
	Déterminer la représentation paramétrique de la droite $(\Delta)$, passant par $S$ et orthogonale au plan $(ABC)$.
	\item On appelle $H$ le point d'intersection de la droite $(\Delta)$ et du plan $(ABC)$.
	
	Montrer que les coordonnées de $H$ sont $(0;-1;2)$.
	\item Calculer la valeur exacte de la distance $SH$.
	\item On considère le cercle $\mathcal{C}$, inclus dans le plan $(ABC)$, de centre $H$, passant par le point $B$. On appelle $\mathcal{D}$ le disque délimité par le cercle $\mathcal{C}$.
	
	Déterminer la valeur exacte de l'aire du disque $\mathcal{D}$.
	\item En déduire la valeur exacte du volume du cône de sommet $S$ et de base le disque $\mathcal{D}$.
\end{enumerate}

\vspace*{5mm}

\section*{Exercice 4 \dotfill(5 points)}

\medskip

\emph{Cet exercice est un questionnaire à choix multiple.\\ Pour chaque question, une seule des quatre réponses proposées est exacte. Le candidat indiquera sur sa copie le numéro de la question et la réponse choisie. Aucune justification n'est demandée.\\ Une réponse fausse, une réponse multiple ou l'absence de réponse à une question ne rapporte ni n'enlève de point.\\ Les cinq questions sont indépendantes.}

\bigskip

Une chaîne de fabrication produit des pièces mécaniques. On estime que 4\,\% des pièces produites par cette chaîne sont défectueuses.

\smallskip

On choisit au hasard $n$ pièces produites par la chaîne de fabrication. Le nombre de pièces produites est suffisamment grand pour que ce choix puisse être assimilé à un tirage avec remise. On note $X$ la variable aléatoire égale au nombre de pièces défectueuses tirées.

\medskip

Dans les trois questions suivantes, on prend $n = 50$.

\begin{enumerate}
	\item Quelle est la probabilité, arrondie au millième, de tirer au moins une pièce défectueuse ?
	\begin{enumerate}
		\item $1$
		\item $0,870$
		\item $0,600$
		\item $0,599$
	\end{enumerate}
	\pagebreak
	\item La probabilité $p(3 < X \leqslant 7)$ est égale à :
	\begin{enumerate}
		\item $p(X \leqslant 7) - p(X > 3)$
		\item $p(X \leqslant 7) - p(X \leqslant 3)$
		\item $p(X < 7) - p(X > 3)$
		\item $p(X < 7) - p(X \geqslant 3)$
	\end{enumerate}
	\item Quel est le plus petit entier naturel $k$ tel que la probabilité de tirer au plus $k$ pièces défectueuses soit supérieure ou égale à 95\,\% ?
	\begin{enumerate}
		\item 2
		\item 3
		\item 4
		\item 5
	\end{enumerate}
\end{enumerate}

Dans les questions suivantes, $n$ ne vaut plus nécessairement 50.

\begin{enumerate}[resume]
	\item Quelle est la probabilité de ne tirer que des pièces défectueuses ?
	\begin{enumerate}
		\item $0,04^n$
		\item $0,96^n$
		\item $1-0,04^n$
		\item $1-0,96^n$
	\end{enumerate}
	\item On considère la fonction \textsf{Python} ci-dessous. Que renvoie-t-elle ?
	
\begin{CodePythonLstAlt}*[Largeur=6cm]{center}
def seuil(x) :
	n = 1
	while 1-0.96**n < x :
		n = n+1
	return n
\end{CodePythonLstAlt}
	\begin{enumerate}
		\item Le plus petit nombre $n$ tel que la probabilité de tirer au moins une pièce défectueuse soit supérieure ou égale à $x$.
		\item Le plus petit nombre $n$ tel que la probabilité de ne tirer aucune pièce défectueuse soit supérieure ou égale à $x$.
		\item Le plus grand nombre $n$ tel que la probabilité de ne tirer que des pièces défectueuses soit supérieure ou égale à $x$.
		\item Le plus grand nombre $n$ tel que la probabilité de ne tirer aucune pièce défectueuse soit supérieure ou égale à $x$.
	\end{enumerate}
\end{enumerate}

\end{document}