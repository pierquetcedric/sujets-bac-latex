% !TeX TXS-program:compile = txs:///pdflatex

\documentclass[french,a4paper,11pt]{article}
\usepackage[margin=2cm,includeheadfoot]{geometry}
\usepackage{ProfLycee}
\RequirePackage[upright]{fourier}
\usepackage{amsmath,amssymb,amstext}
\usepackage[scaled=0.875]{helvet}
\renewcommand\ttdefault{lmtt}
\usepackage{decimalcomma}
\usepackage{esvect}
\usepackage{wrapstuff}
\usepackage{logoetalab}

\usepackage{enumitem}
\setlist[enumerate,1]{label=\bfseries\arabic*.}
\setlist[enumerate,2]{label=\bfseries\alph*.}

\newcommand{\session}{2023}
\newcommand{\annee}{2023}
\newcommand{\serie}{Gé.}
\newcommand{\lieu}{Métropole}
\newcommand{\jour}{20}
\newcommand{\mois}{Mars}
\newcommand{\numsujet}{1}
\newcommand{\nomfichier}{[BAC \serie{} \annee{}] \lieu{} (\mois{} \annee)}

\title{\nomfichier}
\usepackage{hyperref}
\usepackage{lastpage}
\usepackage{enumitem}
\usepackage{ibrackets}
\usepackage{fancyhdr}
\usepackage{babel}

\hypersetup{pdfauthor={Pierquet},pdftitle={\nomfichier},allbordercolors=white,pdfborder=0 0 0,pdfstartview=FitH}
\lhead{\scriptsize\sffamily BAC \serie{} \session}
\chead{\scriptsize\sffamily \lieu{} - Sujet \numsujet}
\rhead{\scriptsize\sffamily \jour{} \mois{} \annee{}}
\lfoot{\scriptsize\sffamily \affloetalab[Legende,TexteLegende={Licence Etalab 2.0}]}
\cfoot{\scriptsize\sffamily 23-MATJ1ME1}
\rfoot{\scriptsize\sffamily - \thepage{}/{}\pageref{LastPage} -}
\setlength{\parindent}{0pt}
%divers
\DeclareMathSymbol{;}\mathbin{operators}{'73}
\newcommand\vect[1]{\vv{#1}}

\begin{document}

\pagestyle{fancy}

{\hfill\Huge \faGraduationCap\hfill~}

\part*{\lieu{}, Bac \serie{}, \jour{} \mois{} \annee, sujet n°\numsujet}

\vspace{0.25cm}

\section*{Exercice 1 \dotfill(5 points)}

\medskip

\emph{Cet exercice est un questionnaire à choix multiple.\\ Pour chaque question, une seule des quatre réponses proposées est exacte. Le candidat indiquera sur sa copie le numéro de la question et la réponse choisie.\\ Aucune justification n’est demandée. \\ Aucun point n’est enlevé en l’absence de réponse ou en cas de réponse inexacte. \\ Les questions sont indépendantes.}

\bigskip

Un technicien contrôle les machines équipant une grande entreprise. Toutes ces machines sont identiques. 

On sait que :

\begin{itemize}
	\item 20\,\% des machines sont sous garantie ;
	\item $0,2$\,\% des machines sont à la fois défectueuses et sous garantie ;
	\item $8,2$\,\% des machines sont défectueuses.
\end{itemize}

\begin{wrapstuff}[r,abovesep=-0.25\baselineskip]
	\def\ArbreDeuxDeux{
	$G$//above,
	//above,
	//above,
	//above,
	//above,
	//above
}
\ArbreProbasTikz[EspaceNiveau=1.85,EspaceFeuille=1.05]{\ArbreDeuxDeux}
\end{wrapstuff}

Le technicien teste une machine au hasard.

\smallskip

On considère les événements suivants :

\begin{itemize}
	\item $G$ : « la machine est sous garantie » ;
	\item $D$ : « la machine est défectueuse » ;
	\item $\overline{G}$ et $\overline{D}$ désignent respectivement les événements contraires de $G$ et $D$.
\end{itemize}

Pour répondre aux questions \textbf{1} à \textbf{3}, on pourra s’aider de l’arbre proposé ci-contre.

\begin{enumerate}
	\item La probabilité $p_G(D)$ de l’événement $D$ sachant que $G$ est réalisé est égale à : 
	
	\smallskip
	
	\begin{tblr}{width=\linewidth,colspec={X[l,m]X[l,m]}}%X[l,m]X[l,m]}}
		\textbf{a.}~~$0,002$ & \textbf{b.}~~$0,01$ \\
		\textbf{c.}~~$0,024$ & \textbf{d.}~~$0,2$
	\end{tblr}
	\item La probabilité $p\big(\overline{G} \cap D\big)$ : 
	
	\smallskip
	
	\begin{tblr}{width=\linewidth,colspec={X[l,m]X[l,m]}}%X[l,m]X[l,m]}}
		\textbf{a.}~~$0,01$ & \textbf{b.}~~$0,08$ \\
		\textbf{c.}~~$0,1$ & \textbf{d.}~~$0,21$
	\end{tblr}
	\item La machine est défectueuse. La probabilité qu’elle soit sous garantie est environ égale, à $10^{-3}$ près, à :
	
	\smallskip
	
	\begin{tblr}{width=\linewidth,colspec={X[l,m]X[l,m]}}%X[l,m]X[l,m]}}
		\textbf{a.}~~$0,01$ & \textbf{b.}~~$0,024$ \\
		\textbf{c.}~~$0,082$ & \textbf{d.}~~$0,1$
	\end{tblr}
\end{enumerate}

Pour les questions \textbf{4} et \textbf{5}, on choisit au hasard et de façon indépendante $n$ machines de l’entreprise, où $n$ désigne un entier naturel non nul. On assimile ce choix à un tirage avec remise, et on désigne par $X$ la variable aléatoire qui associe à chaque lot de $n$ machines le nombre de machines défectueuses dans ce lot. 

On admet que $X$ suit la loi binomiale de paramètres $n$ et $p = 0,082$.

\begin{enumerate}[resume]
	\item Dans cette question, on prend $n=50$.
	
	\smallskip
	
	La valeur de la probabilité $p(X > 2)$, arrondie au millième, est de :
	
	\smallskip
	
	\begin{tblr}{width=\linewidth,colspec={X[l,m]X[l,m]}}%X[l,m]X[l,m]}}
		\textbf{a.}~~$0,136$ & \textbf{b.}~~$0,789$ \\
		\textbf{c.}~~$0,864$ & \textbf{d.}~~$0,924$
	\end{tblr}
	\item On considère un entier $n$ pour lequel la probabilité que toutes les machines d’un lot de taille $n$ fonctionnent correctement est supérieure à $0,4$. La plus grande valeur possible pour $n$ est égale à : 
	
	\smallskip
	
	\begin{tblr}{width=\linewidth,colspec={X[l,m]X[l,m]X[l,m]X[l,m]}}
		\textbf{a.}~~$5$ & \textbf{b.}~~$6$ & \textbf{c.}~~$10$ & \textbf{d.}~~$11$
	\end{tblr}
\end{enumerate}

\vspace{0.5cm}

\section*{Exercice 2 \dotfill(5 points)}

\medskip

On considère la fonction $f$ définie sur $]0;+\infty[$ par $f(x)=x^2-8\,\ln(x)$, où $\ln$ désigne la fonction logarithme népérien.

\smallskip

On admet que $f$ est dérivable sur $]0;+\infty[$, on note $f'$ sa fonction dérivée.

\begin{enumerate}
	\item Déterminer $\displaystyle\lim_{x \to 0} f(x)$.
	\item On admet que, pour tout réel $x$ de $]0;+\infty[$, $f(x)=x^2 \left(1-8\frac{\ln(x)}{x}\right)$.
	
	Déterminer $\displaystyle\lim_{x \to +\infty} f(x)$.
	\item Montrer que, pour tout réel $x$ de $]0;+\infty[$, $f'(x)=\frac{2\big(x^2-4\big)}{x}$.
	\item Étudier les variations de $f$ sur $]0;+\infty[$ et dresser son tableau de variations complet.
	
	On précisera la valeur exacte du minimum de $f$ sur $]0;+\infty[$.
	\item Démontrer que, sur l'intervalle $]0;2]$, l’équation $f(x)=0$ admet une solution unique $\alpha$ (on ne cherchera pas à déterminer la valeur de $\alpha$).
	\item On admet que, sur l’intervalle $[2;+\infty[$, l’équation $f(x)=0$ admet une solution unique $\beta$ (on ne cherchera pas à déterminer la valeur de $\beta$).
	
	En déduire le signe de $f$ sur l’intervalle $]0;+\infty[$.
	\item Pour tout nombre réel $k$, on considère la fonction $g_k$ définie sur $]0;+\infty[$ par : \[ g_k(x)=x^2-8\,\ln(x)+k. \]
	%
	En s’aidant du tableau de variations de $f$, déterminer la plus petite valeur de $k$ pour laquelle la fonction $g_k$ est positive sur l’intervalle $]0;+\infty[$.
\end{enumerate}

\pagebreak

\section*{Exercice 3 \dotfill(5 points)}

\medskip

Une entreprise a créé une Foire Aux Questions (« FAQ ») sur son site internet.

\smallskip

On étudie le nombre de questions qui y sont posées chaque mois. 

\bigskip

\textbf{Partie A : Première modélisation}

\medskip

Dans cette partie, on admet que, chaque mois :

\begin{itemize}
	\item 90\,\% des questions déjà posées le mois précédent sont conservées sur la FAQ ;
	\item 130 nouvelles questions sont ajoutées à la FAQ.
\end{itemize}

Au cours du premier mois, 300 questions ont été posées. 

\smallskip

Pour estimer le nombre de questions, en centaines, présentes sur la FAQ le $n$-ième mois, on modélise la situation ci-dessus à l’aide de la suite $\left(u_n\right)$ définie par :%
\[ u_1=3 \text{ et, pour tout entier naturel } n \geqslant 1\text{, } u_{n+1}=0,9u_n+1,3. \]
%
\begin{enumerate}
	\item Calculer $u_2$ et $u_3$ et proposer une interprétation dans le contexte de l’exercice.
\begin{wrapstuff}[r,abovesep=0.75\baselineskip]
\begin{CodePythonLstAlt}*[Largeur=5.5cm]{fontupper=\scriptsize}
def seuil(p) :
	n = 1
	u = 3
	while u <= p :
		n = n+1
		u = 0.9*u + 1.3
	return n
\end{CodePythonLstAlt}
\end{wrapstuff}
	\item Montrer par récurrence que pour tout entier naturel $n\geqslant 1$ :%
	\[ u_{n} = 13 - \frac{100}{9} \times 0,9^n. \]
	\item En déduire que la suite $\left(u_n\right)$ est croissante.
	\item On considère le programme ci-contre, écrit en langage \textsf{Python}.
	
	\smallskip
	
	Déterminer la valeur renvoyée par la saisie de \texttt{seuil(8.5)} et l’interpréter dans le contexte de l’exercice. 
\end{enumerate}

\smallskip

\textbf{Partie B : Une autre modélisation}

\medskip

Dans cette partie, on considère une seconde modélisation à l’aide d’une nouvelle suite $\left(v_n\right)$ définie pour tout entier naturel $n \geqslant 1$ par :
%
\[ v_n = 9 - 6 \times \text{e}^{-0,19 \times (n-1)} .\]
%
Le terme $v_n$ est une estimation du nombre de questions, en centaines, présentes le $n$-ième mois sur la FAQ.

\begin{enumerate}
	\item Préciser les valeurs arrondies au centième de $v_1$ et $v_2$.
	\item Déterminer, en justifiant la réponse, la plus petite valeur de $n$ telle que $v_n > 8,5$.
\end{enumerate}

\smallskip

\textbf{Partie C : Comparaison des deux modèles}

\smallskip

\begin{enumerate}
	\item L’entreprise considère qu’elle doit modifier la présentation de son site lorsque plus de 850 questions sont présentes sur la FAQ. Parmi ces deux modélisations, laquelle conduit à procéder le plus tôt à cette modification ? 
	
	Justifier votre réponse. 
	\item En justifiant la réponse, pour quelle modélisation y a-t-il le plus grand nombre de questions sur la FAQ à long terme ? 
\end{enumerate}

\pagebreak

\section*{Exercice 4 \dotfill(5 points)}

\medskip

On considère le cube $ABCDEFGH$ d’arête 1.

On appelle $I$ le point d’intersection du plan $(GBD)$ avec la droite $(EC)$.

\begin{center}
	\begin{tikzpicture}[x={(0:4cm)},y={(27:2cm)},z={(90:4cm)},line join=bevel]
		\coordinate (A) at (0,0,0) ; \coordinate (B) at (1,0,0) ;
		\coordinate (C) at (1,1,0) ; \coordinate (D) at (0,1,0) ;
		\coordinate (E) at (0,0,1) ; \coordinate (F) at (1,0,1) ;
		\coordinate (G) at (1,1,1) ; \coordinate (H) at (0,1,1) ;
		\coordinate (I) at ({2/3},{2/3},{1/3}) ;
		\coordinate (J) at ($(D)!0.5!(B)$) ;
		\coordinate (U) at ($(E)!-0.16!(C)$) ;%prolongement de (EC) à gauche
		\coordinate (V) at ($(E)!1.19!(C)$) ;%prolongement de (EC) à droite
		\draw[semithick,dashed] (A)--(D)--(C) (D)--(H) ;
		\draw[semithick,densely dashed] (D)--(B) (E)--(C) (D)--(G) ;
		\draw[semithick] (A)--(B)--(F)--(E)--cycle  (B)--(C)--(G)--(F)--cycle (E)--(F)--(G)--(H)--cycle (B)--(G) (U)--(E) (C)--(V) ;
		\foreach \point/\pos in {A/below,B/below,C/below,D/below,E/above,F/above,G/above,H/above,I/below left,J/below left}
			{\filldraw (\point) circle[radius=1.25pt] node[\pos] {$\point$} ;}
	\end{tikzpicture}
\end{center}

L’espace est rapporté au repère $\left(A;\vect{AB},\,\vect{AD},\,\vect{AE}\right)$.

\begin{enumerate}
	\item Donner dans ce repère les coordonnées des points $E$, $C$, $G$.
	\item Déterminer une représentation paramétrique de la droite $(EC)$.
	\item Démontrer que la droite $(EC)$ est orthogonale au plan $(GBD)$.
	\item 
	\begin{enumerate}
		\item Justifier qu’une équation cartésienne du plan $(GBD)$ est : $x + y - z - 1 = 0$.
		\item Montrer que le point $I$ a pour coordonnées $\left(\frac23;\frac23;\frac13\right)$.
		\item En déduire que la distance du point $E$ au plan $(GBD)$ est égale à $\frac{2\sqrt{3}}{3}$.
	\end{enumerate}
	\item 
	\begin{enumerate}
		\item Démontrer que le triangle $BDG$ est équilatéral.
		\item Calculer l’aire du triangle $BDG$. On pourra utiliser le point $J$, milieu du segment $[BD]$.
	\end{enumerate}
	\item Justifier que le volume du tétraèdre $EGBD$ est égal à $\frac13$.
	
	\smallskip
	
	\textit{On rappelle que le volume d’un tétraèdre est donné par : $\mathcal{V}=\frac13 \mathcal{B}h$ où $\mathcal{B}$ est l’aire d’une base du tétraèdre et $h$ est la hauteur relative à cette base.}
\end{enumerate}

\end{document}