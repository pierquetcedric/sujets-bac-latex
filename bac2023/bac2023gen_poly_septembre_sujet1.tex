% !TeX TXS-program:compile = txs:///pdflatex

\documentclass[french,a4paper,11pt]{article}
\usepackage[margin=2cm,includeheadfoot]{geometry}
\usepackage{ProfLycee}
\useproflyclib{ecritures}
\RequirePackage[upright]{fourier}
\usepackage{amsmath,amssymb,amstext}
\usepackage[scaled=0.875]{helvet}
\renewcommand\ttdefault{lmtt}
\usepackage{esvect}
\usepackage{customenvs}
\usepackage{logoetalab}

\newcommand{\session}{2023}
\newcommand{\annee}{2023}
\newcommand{\serie}{Gé.}
\newcommand{\lieu}{Polynésie}
\newcommand{\jour}{7}
\newcommand{\mois}{Septembre}
\newcommand{\numsujet}{1}
\newcommand{\nomfichier}{[BAC \serie{} \annee{}] \lieu{} (\mois{} \annee)}

\title{\nomfichier}
\usepackage{hyperref}
\usepackage{lastpage}
\usepackage{enumitem}
\usepackage{ibrackets}
\usepackage{fancyhdr}
\usepackage{babel}

\hypersetup{pdfauthor={Pierquet},pdftitle={\nomfichier},allbordercolors=white,pdfborder=0 0 0,pdfstartview=FitH}
\lhead{\scriptsize\sffamily BAC \serie{} \session}
\chead{\scriptsize\sffamily \lieu{} - Sujet \numsujet}
\rhead{\scriptsize\sffamily \jour{} \mois{} \annee{}}
\lfoot{\scriptsize\sffamily \affloetalab[Legende,TexteLegende={Licence Etalab 2.0}]}
\cfoot{\scriptsize\sffamily 23-MATJ1PO3}
\rfoot{\scriptsize\sffamily - \thepage{}/{}\pageref{LastPage} -}
\setlength{\parindent}{0pt}
%divers
\DeclareMathSymbol{;}\mathbin{operators}{'73}
\newcommand\vect[1]{\vv{#1}}

\begin{document}

\pagestyle{fancy}

{\hfill\Huge \faGraduationCap\hfill~}

\part*{\lieu{}, Bac \serie{}, \jour{} \mois{} \annee, sujet n°\numsujet}

\vspace{0.25cm}

\section*{Exercice 1 [Probabilités]\dotfill(4 points)}

\medskip

Une concession automobile vend des véhicules à moteur électrique et des véhicules à moteur thermique.

Certains clients, avant de se rendre sur le site de la concession, ont consulté la plate-forme numérique de la concession. On a ainsi observé que :

\begin{itemize}
	\item 20\,\% des clients sont intéressés par les véhicules à moteur électrique et 80\,\% préfèrent s'orienter vers l'achat d'un véhicule à moteur thermique ;
	\item lorsqu'un client souhaite acheter un véhicule à moteur électrique, la probabilité pour que le client ait consulté la plate-forme numérique est de $0,5$ ;
	\item lorsqu'un client souhaite acheter un véhicule à moteur thermique, la probabilité pour que le client ait consulté la plate-forme numérique est de $0,375$.
\end{itemize}

On considère les événements suivants :

\begin{itemize}
	\item $C$ : « un client a consulté la plate-forme numérique » ;
	\item $E$ : « un client souhaite acquérir un véhicule à moteur électrique » ;
	\item $T$ : « un client souhaite acquérir un véhicule à moteur thermique ».
\end{itemize}

Les clients font des choix indépendants les uns des autres.

\begin{enumerate}
	\item 
	\begin{enumerate}
		\item Calculer la probabilité qu'un client choisi au hasard souhaite acquérir un véhicule à moteur électrique et ait consulté la plate-forme numérique.
		
		On pourra utiliser un arbre pondéré.
		\item Démontrer que $P(C)=0,4$.
		\item On suppose qu'un client a consulté la plate-forme numérique.
		
		Calculer la probabilité que le client souhaite acheter un véhicule à moteur électrique.
	\end{enumerate}
	\item La concession accueille quotidiennement 17 clients en moyenne.
	
	On note $X$ la variable aléatoire donnant le nombre de clients souhaitant acquérir
	un véhicule à moteur électrique.
	\begin{enumerate}
		\item Préciser la nature et les paramètres de la loi de probabilité suivie par $X$.
		\item Calculer la probabilité qu'au moins trois des clients souhaitent acheter un
		véhicule à moteur électrique lors d'une journée. Donner le résultat arrondi à $10^{-2}$ près.
	\end{enumerate}
\end{enumerate}

\pagebreak

\section*{Exercice 2 [Fonctions]\dotfill(6 points)}

\medskip

\textit{Les parties A et B peuvent être traitées indépendamment.}

\medskip

\textbf{Partie A}

\medskip

On considère la fonction $f$ définie sur $\R$ par : \[ f(x)=\left(x+\dfrac12\right)\e^{-x}+x. \]

\begin{enumerate}
	\item Déterminer les limites de $f$ en $-\infty$ et en $+\infty$.
	\item On admet que $f$ est deux fois dérivable sur $\R$.
	\begin{enumerate}
		\item Démontrer que, pour tout $x \in \R$, \[ f''(x)=\left(x-\dfrac32\right)\e^{-x}. \]
		\item En déduire les variations et le minimum de la fonction $f'$ sur $\R$.
		\item Justifier que pour tout $x \in \R$, $f'(x)>0$.
		\item En déduire que l'équation $f(x)=0$ admet une unique solution sur $\R$.
		\item Donner une valeur arrondie à $10^{-3}$ de cette solution.
	\end{enumerate}
\end{enumerate}

\medskip

\textbf{Partie B}

\medskip

On considère une fonction $h$, définie et dérivable sur $\R$, ayant une expression de la forme \[ h(x)=(ax+b)\e^{-x}+x,\] où $a$ et $b$ sont deux réels.

Dans un repère orthonormé ci-après figurent :

\begin{itemize}
	\item la courbe représentative de la fonction $h$ ;
	\item les points $A$ et $B$ de coordonnées respectives $(-2;-2,5)$ et $(2;3,5)$.
\end{itemize}

\begin{center}
	\begin{tikzpicture}[x=0.75cm,y=0.75cm,xmin=-7,xmax=6,xgrille=1,xgrilles=0.2,ymin=-5,ymax=6,ygrille=1,ygrilles=0.2]
		\GrilleTikz\AxesTikz
		\AxexTikz[AffOrigine=false,Police=\small]{-6,-5,...,6}
		\AxeyTikz[AffOrigine=false,Police=\small]{-5,-4,...,6}
		\draw (-6pt,-4pt) node[below] {$0$} ;
		\clip (\xmin,\ymin) rectangle (\xmax,\ymax) ;
		\CourbeTikz[line width=1.25pt,blue,samples=2]{1.5*\x+0.5}{\xmin:\xmax}
		\filldraw[blue] (-2,-2.5) circle[radius=2pt] node[right] {$A$} ;
		\filldraw[blue] (2,3.5) circle[radius=2pt] node[right] {$B$} ;
		\CourbeTikz[line width=1.25pt,red,samples=250]{(\x+0.5)*exp(-\x)+\x}{-1.5:6}
	\end{tikzpicture}
\end{center}

\begin{enumerate}
	\item Conjecturer, avec la précision permise par le graphique, les abscisses des éventuels points d'inflexion de la courbe représentative de la fonction $h$.
	\item Sachant que la fonction $h$ admet sur $\R$ une dérivée seconde d'expression \[ h''(x)=-\dfrac32\e^{-x}+x\,\e^{-x},\]%
	valider ou non la conjecture précédente.
	\item Déterminer une équation de la droite $(AB)$.
	\item Sachant que la droite $(AB)$ est tangente à la courbe représentative de la fonction $h$ au point d'abscisse $0$, en déduire les valeurs de $a$ et $b$.
\end{enumerate}

\pagebreak

\section*{Exercice 3 [Suites, algorithmique]\dotfill(5 points)}

\medskip

On considère la fonction $f$ définie sur $\R$ par \[ f(x)=\dfrac34x^2-2x+3. \]
%
\begin{enumerate}
	\item Dresser le tableau de variations de $f$ sur $\R$.
	\item En déduire, que pour tout $x$ appartenant à l'intervalle $\IntervalleFF{\frac43}{2}$, $f(x)$ appartient à l'intervalle $\IntervalleFF{\frac43}{2}$.
	\item Démontrer que pour tout $x$ réel, $x \leqslant f(x)$.
	
	Pour cela, on pourra démontrer que pour tout réel $x$ : \[ f(x)-x = \dfrac34(x-2)^2. \]
\end{enumerate}


On considère la suite $\Suite{u}$ définie par un réel $u_0$ et pour tout entier naturel $n$ : \[ u_{n+1} = f\big(u_n\big).\]
%
On a donc, pour tout entier naturel $n$, $u_{n+1} = \dfrac34u_n^2-2u_n+3.$

\begin{enumerate}[resume]
	\item Étude du cas : $\frac34 \leqslant u_0 \leqslant 2$.
	\begin{enumerate}
		\item Démontrer par récurrence que, pour tout entier naturel $n$, \[ u_{n+1} \leqslant u_n \leqslant 2. \]
		\item En déduire que la suite $\Suite{u}$ est convergente.
		\item Prouver que la limite de la suite est égale à 2.
	\end{enumerate}
	\item Étude duc as particulier $u_0=3$.
	
	On admet que dans ce cas la suite $\Suite{u}$ tend vers $+\infty$.
	
	Recopier et compléter la fonction « \texttt{seuil} » suivante écrite en \textsf{Python}, afin qu'elle renvoie la plus petite valeur de $n$ telle que $u_n$ soit supérieur ou égal à 100.
	
\begin{CodePythonLstAlt}*[Largeur=7cm]{center}
def seuil() :
	u = 3
	n = 0
	while ......... :
		u = .........
		n = .........
	return n
\end{CodePythonLstAlt}
	\item Étude du cas : $u_0 > 2$.
	
	À l'aide d'un raisonnement par l'absurde, montrer que $\Suite{u}$ n'est pas convergente.
\end{enumerate}

\pagebreak

\section*{Exercice 4 [Géométrie dans l'espace]\dotfill(5 points)}

\medskip

\emph{Cet exercice est un questionnaire à choix multiples.\\ Pour chacune des questions suivantes, une seule des quatre réponses proposées est exacte.\\ Pour répondre, indiquer sur la copie le numéro de la question et la lettre de la réponse choisie.\\ Aucune justification n'est demandée.\\ Une réponse fausse, une absence de réponse, ou une réponse multiple, ne rapporte ni n'enlève de point.}

\medskip

L'espace est muni d'un repère orthonormé $\RepereOijk$ dans lequel on considère :

\begin{itemize}
	\item les points $A(6;-6;6)$, $B(-6;0;6)$ et $C(-2;-2;11)$ ;
	\item la droite $(d)$ orthogonale aux deux droites sécantes $(AB)$ et $(BC)$ et passant par le point $A$ ;
	\item la droite $(d')$ de représentation paramétrique : \[ \begin{dcases}x=-6-8t\\y=4t\\z=6+5t\end{dcases}\text{, avec } t \in \R.\]
\end{itemize}

\medskip

\textbf{Question 1}

\medskip

Parmi les vecteurs suivants, lequel est un vecteur directeur de la droite $(d)$ ?

\smallskip

\ReponsesQCM[Labels=(a),EspaceLabels={~~~},PoliceLabels={}]{%
	$\Vecteur*{u}[1]  \CoordVecEsp{-6}{3}{0}$ §
	$\Vecteur*{u}[2]  \CoordVecEsp{1}{2}{6}$ §
	$\Vecteur*{u}[3]  \CoordVecEsp{1}{2}{0,2}$ §
	$\Vecteur*{u}[4]  \CoordVecEsp{1}{2}{0}$
}

\medskip

\textbf{Question 2}

\medskip

Parmi les équations suivantes, laquelle est une représentation paramétrique de la droite
$(AB)$ ?

\smallskip

\ReponsesQCM[NbCols=2,Labels=(a),EspaceLabels={~~~},PoliceLabels={},Swap]{%
	$\begin{dcases}x=2t-6\\y=-6\\z=t+6\end{dcases}\text{, avec } t \in \R.$ §
	$\begin{dcases}x=2t-6\\y=-6\\z=-t-6\end{dcases}\text{, avec } t \in \R.$ §
	$\begin{dcases}x=2t+6\\y=-t-6\\z=6\end{dcases}\text{, avec } t \in \R.$ §
	$\begin{dcases}x=2t+6\\y=t-6\\z=6\end{dcases}\text{, avec } t \in \R.$
}

\medskip

\textbf{Question 3}

\medskip

Un vecteur directeur de la droite $(d')$ est :

\smallskip

\ReponsesQCM[Labels=(a),EspaceLabels={~~~},PoliceLabels={}]{%
	$\Vecteur*{v}[1]  \CoordVecEsp{-6}{0}{6}$ §
	$\Vecteur*{v}[2]  \CoordVecEsp{-14}{4}{11}$ §
	$\Vecteur*{v}[3]  \CoordVecEsp{8}{-4}{-5}$ §
	$\Vecteur*{v}[4]  \CoordVecEsp{8}{-4}{5}$
}

\medskip

\textbf{Question 4}

\medskip

Lequel des quatre points suivants appartient à la droite $(d')$ ?

\smallskip

\ReponsesQCM[Labels=(a),EspaceLabels={~~~},PoliceLabels={},NbCols=2,Swap]{%
	$M_1(50;-28;-29)$ §
	$M_2(-14;-4;11)$ §
	$M_3(2;-4;-1)$ §
	$M_4(-3;0;3)$
}

\medskip

\textbf{Question 5}

\medskip

Le plan d'équation $x=1$ a pour vecteur normal :

\smallskip

\ReponsesQCM[Labels=(a),EspaceLabels={~~~},PoliceLabels={}]{%
	$\Vecteur*{n}[1]  \CoordVecEsp{1}{0}{0}$ §
	$\Vecteur*{n}[2]  \CoordVecEsp{0}{1}{1}$ §
	$\Vecteur*{n}[3]  \CoordVecEsp{0}{1}{0}$ §
	$\Vecteur*{n}[4]  \CoordVecEsp{1}{0}{1}$
}

\end{document}